<?
use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Барк «Крузенштерн» виртуальный тур");
$APPLICATION->SetTitle("Барк «Крузенштерн» виртуальный тур");

?>
<div id="virtual">
    <embed style="display:block; " type="application/x-shockwave-flash" src="http://kruzenshtern.info/virtual_tour/bark_kruzenshtern_rus/flash/twviewer.swf" width="100%" height="700px" id="sotester" name="sotester" bgcolor="#1463B0" quality="high" allownetworking="all" allowscriptaccess="always" allowfullscreen="true" scale="noscale" flashvars="lwImg=resources/bark_kruzenshtern_zagruzka_upravlenie.gif&amp;lwBgColor=255,42,169,224&amp;lwBarBgColor=255,255,255,255&amp;lwBarColor=255,246,191,35&amp;lwBarBounds=-129,3,260,5&amp;lwlocation=4&amp;lwShowLoadingPercent=false&amp;lwTextColor=255,160,202,66&amp;iniFile=config_bark_kruzenshtern.bin&amp;progressType=0&amp;swfFile=&amp;percentType=0&amp;sizeFile=filesize.txt&amp;href=http://kruzenshtern.info/virtual_tour/bark_kruzenshtern_rus/flash/pixiq_bark_kruzenshtern.html">
</div>
<script>

    function wheel(event){

        if(event.target.id == 'sotester') {
            var delta = 0;
            if (!event) /* For IE. */
                event = window.event;
            if (event.wheelDelta) { /* IE/Opera. */
                delta = event.wheelDelta / 120;
            } else if (event.detail) { /** Mozilla case. */
                /** In Mozilla, sign of delta is different than in IE.
                 * Also, delta is multiple of 3.
                 */
                delta = -event.detail / 3;
            }
            /** If delta is nonzero, handle it.
             * Basically, delta is now positive if wheel was scrolled up,
             * and negative, if wheel was scrolled down.
             */
            window.scrollBy(0,-delta*30);

            /** Prevent default actions caused by mouse wheel.
             * That might be ugly, but we handle scrolls somehow
             * anyway, so don't bother here..
             */


            if (event.preventDefault)
                event.preventDefault();
            event.returnValue = false;
        }
    }

    $(document).ready(function(){
        if (window.addEventListener)
            /** DOMMouseScroll is for mozilla. */
            window.addEventListener('DOMMouseScroll', wheel, false);
        /** IE/Opera. */
        window.onmousewheel = document.onmousewheel = wheel;



    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>