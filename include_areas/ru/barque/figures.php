<br/>
<aside class="additional-figures center-wrapper">
    <div class="additional-figures__more">а также:</div>
    <div class="additional-figures__txt">
        <span class="additional-figures__number">425</span> суток - самый длительный рейс (2-я кругосветка 2005-2006 годов)
    </div>
    <div class="additional-figures__txt">
        <span class="additional-figures__number">30</span> суток - автономное плавание (по запасу продуктов),
        <span class="additional-figures__number">711</span> тонн - запас пресной воды
    </div>
    <div class="additional-figures__txt">
        <span class="additional-figures__number">516</span> тонн - запас топлива, <span class="additional-figures__number">250</span> м - длина якорной цепи,
        <span class="additional-figures__number">3</span> тонны - вес якоря Холла
    </div>
</aside>

<br/><br/><br/><br/><br/><br/>
