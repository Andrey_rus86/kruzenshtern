<?
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Tale\Jade;
use Bitrix\Main\Application;
use Keypoint\Utils\Iblock;
use PHPHtmlParser\Dom;


include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//include 'simple_html_dom.php';

$appContext = Application::getInstance()->getContext();
$server = $appContext->getServer();
# Автозагрузчик composer
include_once ($server->getDocumentRoot() . '/local/php_interface/classes/vendor/autoload.php');


Loader::includeModule('iblock');

if(!$USER->isAuthorized()) exit;


$files = scandir('./photos');

foreach($files as $file) {
    if($file == '.' || $file == '..') continue;
    $pathInfo = pathinfo('./photos/'.$file);
    $pathInfo['filename'] = iconv('Windows-1251','UTF-8',$pathInfo['filename']);

    if($pathInfo['extension'] != 'JPG' && $pathInfo['extension'] != 'jpg') continue;

    $el = new CIBlockElement;
    $photoElem = Array(
        "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
        "IBLOCK_SECTION_ID" => 439,
        "IBLOCK_ID"      => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
        "NAME"           => capitalize_first($pathInfo['filename']),
        //'PREVIEW_TEXT' => capitalize_first($pathInfo['filename']).'. Фотограф - Василий Семидьянов.',
        "ACTIVE"         => "Y",
        "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/tools/import_to_album/photos/'.$file),
        "PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'].'/tools/import_to_album/photos/'.$file),
    );

    $el->Add($photoElem);

    unlink('./photos/'.$file);
}

function capitalize_first($str) {
    $line = iconv("UTF-8", "Windows-1251", $str); // convert to windows-1251
    $line = ucfirst($line);
    $line = iconv("Windows-1251", "UTF-8", $line); // convert back to utf-8

    return $line;
}