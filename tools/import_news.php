<?
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Tale\Jade;
use Bitrix\Main\Application;
use Keypoint\Utils\Iblock;
use PHPHtmlParser\Dom;


include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include 'simple_html_dom.php';

$appContext = Application::getInstance()->getContext();
$server = $appContext->getServer();
# Автозагрузчик composer
include_once ($server->getDocumentRoot() . '/local/php_interface/classes/vendor/autoload.php');


Loader::includeModule('iblock');

define('DOMAIN', 'http://www.bgarf.ru');

if(!$USER->isAuthorized()) exit;

function getCurlQuery($addr) {
    $uagent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.14";

    $ch = curl_init( $addr );

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
    curl_setopt($ch, CURLOPT_HEADER, 0);           // не возвращает заголовки
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);   // переходит по редиректам
    curl_setopt($ch, CURLOPT_ENCODING, '');        // обрабатывает все кодировки
    curl_setopt($ch, CURLOPT_USERAGENT, $uagent);  // useragent
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);        // таймаут ответа
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);       // останавливаться после 10-ого редиректа

    $content = curl_exec( $ch );

    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}

$addr = 'http://www.bgarf.ru/kruzenshtern/?SHOWALL_1=1';
$html = getCurlQuery($addr);

// Получаем список новостей на странице
preg_match_all('#<article class="middle news-block krusenshtern".*?>.*?<a href="(.*?)".*?>.*?</a>.*?<span>([0-9]{2}\.[0-9]{2}\.[0-9]{4}).*?</span>.*?<a.*?>(.*?)</a>.*?<p>(.*?)</p>.*?</article>#ims',$html['content'], $matches);
var_dump($matches); exit;

foreach($matches[1] as $key => $link) {
    // Переходим в каждую новость
    $newsPage = getCurlQuery(DOMAIN.$link);

    $resAlbums = SectionTable::getList([
        'select' => ['ID'],
        'filter' => ['=NAME'=>$matches[3][$key], 'IBLOCK_ID' => Iblock::getIblockIdByCode('gallery')]
    ]);

    $sectionID = $resAlbums->fetch();

    if($resAlbums->getSelectedRowsCount()>0) continue;

    // Создаем альбом
   $resSection = SectionTable::add([
        'MODIFIED_BY'    => $USER->GetID(),
        'TIMESTAMP_X' => new DateTime(),
        'NAME' => $matches[3][$key],
        'IBLOCK_ID' => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
        'IBLOCK_SECTION_ID' => 16 // Альбомы к новостям
    ]);

    if ($resSection->isSuccess())
    {
        $sectionID = $resSection->getId();
    } else {
        exit; break;
    }


    // Парсинг фотографий
    preg_match_all('#<div class="item".*?>.*?<a class="fancybox".*?href="(.*?)".*?>.*?</a>.*?</div>#ims',$newsPage['content'], $photoMatches);
    // парсинг новости

    preg_match_all('#<body>(.*?)</body>#ims',$newsPage['content'], $newsDetailMatches);



    $doc = new \DOMDocument();
    //$doc->loadHTML(mb_convert_encoding($newsDetailMatches[1][0], 'HTML-ENTITIES', "UTF-8"));

    $doc->loadHTML('<?xml encoding="UTF-8">' . $newsDetailMatches[1][0]);

    foreach ($doc->childNodes as $item)
        if ($item->nodeType == XML_PI_NODE)
            $doc->removeChild($item);
    $doc->encoding = 'UTF-8'; // reset original encoding

    $xpath = new \DOMXpath($doc);
    $articles = $xpath->query('//div[@class="newstext"]');

    // all links in .blogArticle
    $links = array();
    $fullDescription = '';
    foreach($articles as $container) {
        $fullDescription .= get_inner_html($container);
    }


    // Созраняем фотографии в альбом
    $firstPhoto = null;
    foreach($photoMatches[1] as $photoKey => $photoLink) {
        if($photoKey==0) {
            $firstPhoto = CFile::MakeFileArray(DOMAIN.$photoLink);
        }

        $el = new CIBlockElement;
        $photoElem = Array(
            "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => $sectionID,          // элемент лежит в корне раздела
            "IBLOCK_ID"      => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
            "NAME"           => 'Фото',
            "ACTIVE"         => "Y",
            "DETAIL_PICTURE" => CFile::MakeFileArray(DOMAIN.$photoLink),
            "PREVIEW_PICTURE" => CFile::MakeFileArray(DOMAIN.$photoLink),
        );

        $el->Add($photoElem);

    }


    // Сохраняем новость
    $PROP = [];
    $PROP['ALBUM'] = $sectionID;

    $el = new CIBlockElement;
    $newsElem = Array(
        "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
        "IBLOCK_SECTION_ID" => 20,          // элемент лежит в корне раздела
        "IBLOCK_ID"      => Keypoint\Utils\Iblock::getIblockIdByCode('news'),
        "NAME"           => $matches[3][$key],
        "ACTIVE_FROM"           => $matches[2][$key],
        "PREVIEW_TEXT"          => $matches[4][$key],
        "PREVIEW_TEXT_TYPE"          => 'html',
        "PROPERTY_VALUES"=> $PROP,
        "DETAIL_TEXT"          => $fullDescription,
        "DETAIL_TEXT_TYPE"          => 'html',
        "ACTIVE"         => "Y",
        "DETAIL_PICTURE" => $firstPhoto,
        "PREVIEW_PICTURE" => $firstPhoto,
    );

    $el->Add($newsElem);

}

function get_inner_html( $node ) {
    $innerHTML= '';
    $children = $node->childNodes;
    foreach ($children as $child) {
        $innerHTML .= $child->ownerDocument->saveXML( $child );
    }

    return $innerHTML;  }



