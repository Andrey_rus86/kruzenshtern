<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 07.09.2016
 * Time: 19:46
 */


use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Keypoint\Utils\Iblock;

include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
Loader::includeModule('iblock');

define('DOMAIN', 'http://www.bgarf.ru');

if(!$USER->isAuthorized()) exit;

$resNews = ElementTable::getList([
    'select' => ['*'],
   'filter' => ['IBLOCK_ID' => Iblock::getIblockIdByCode('news')]
]);

while($dataNews = $resNews->fetch()) {


    $yourText = preg_replace('#<.{0,1}div>#ims','', $dataNews['DETAIL_TEXT']);

    $el = new CIBlockElement;

    $arLoadProductArray = Array(
        "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
        "DETAIL_TEXT"    => $yourText
    );

    $res = $el->Update($dataNews['ID'], $arLoadProductArray);

    var_dump($dataNews['ID']);
    break;
}

