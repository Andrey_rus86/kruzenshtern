<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/projects/([0-9]+)/#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/projects/detail/index.php",
	),
	array(
		"CONDITION" => "#^/news/([0-9]+)/#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/news/detail/index.php",
	),
	array(
		"CONDITION" => "#^/gallery/([0-9a-zA-Z\-\_]+)/#",
		"RULE" => "SECTION_CODE=\$1",
		"ID" => "",
		"PATH" => "/gallery/index.php",
	),
	array(
		"CONDITION" => "#^/history/([0-9a-zA-Z\-\_]+)/#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "",
		"PATH" => "/history/index.php",
	),
);

?>