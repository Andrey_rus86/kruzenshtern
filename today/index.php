<?
use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Type\Date;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Барк «Крузенштерн» сегодня");
$APPLICATION->SetTitle("Барк «Крузенштерн» сегодня");

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH .'/css/gallery.css');
?>

<?
ob_start();
$APPLICATION->IncludeFile(
    "/include_areas/".LANGUAGE_ID."/today/intro.php",
    Array(),
    Array("MODE"=>"html")
);
$intro = ob_get_clean();
ob_start();
$APPLICATION->IncludeFile(
    "/include_areas/".LANGUAGE_ID."/today/title.php",
    Array(),
    Array("MODE"=>"html")
);
$title = ob_get_clean();

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/today-text/today-text.jade',
    [
        'todaytext' => [
            'TITLE' => $title,
            'TEXT' => $intro
        ]
    ]);

?>
    <div id="coordinates"></div>
<? /* Карта координат барка */ ?>
<?$APPLICATION->IncludeComponent(
	"keypoint:barque.coordinates", 
	"map",
	array(
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "Y",
		"COORDINATES_PARSER" => "ParserMarineTraffic"
	),
	false
);?>

    <div id="events-list"></div>
<?
ob_start();
$APPLICATION->IncludeFile(
    "/include_areas/".LANGUAGE_ID."/today/past_events_intro.php",
    Array(),
    Array("MODE"=>"html")
);
$intro = ob_get_clean();
ob_start();
$APPLICATION->IncludeFile(
    "/include_areas/".LANGUAGE_ID."/today/past_events_title.php",
    Array(),
    Array("MODE"=>"html")
);
$title = ob_get_clean();

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/events-list-title/events-list-title.jade',
    [
        'events' => [
            'TITLE' => $title,
            'TEXT' => $intro
        ]
    ]);

?>
<?
$filterPastEvents = ['<DATE_ACTIVE_FROM' => (new Date())->format('d.m.Y')];
?>
<?$APPLICATION->IncludeComponent("bitrix:news.list","news-list",Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => Keypoint\Utils\Iblock::getIblockIdByCode('news'),
        "NEWS_COUNT" => "4",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "filterPastEvents",
        "FIELD_CODE" => Array("PREVIEW_PICTURE", "PREVIEW_TEXT"),
        "PROPERTY_CODE" => Array("YEAR"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "Y",
        "SET_BROWSER_TITLE" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y",
        "SET_LAST_MODIFIED" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "ru",
        "INCLUDE_SUBSECTIONS" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "SET_STATUS_404" => "Y",
        "SHOW_404" => "Y",
        "MESSAGE_404" => "",
        "PAGER_BASE_LINK" => "/news/",
        "PAGER_PARAMS_NAME" => "arrPager",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => ""
    )
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>