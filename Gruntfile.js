// Gruntfile with the configuration of grunt-express and grunt-open. No livereload yet!

var path = 'source/app';
var path_build = 'source/build';
var dataJSON = {};

var _getAllFilesFromFolder = function(dir) {

    var filesystem = require("fs");
    var results = [];

    filesystem.readdirSync(dir).forEach(function(file) {

        file = dir+'/'+file;
        var stat = filesystem.statSync(file);

        if (stat && stat.isDirectory()) {
            results = results.concat(_getAllFilesFromFolder(file))
        } else results.push(file);

    });

    return results;

};

module.exports = function(grunt) {

    // Строим объект, содержащий данные JSON файлов
    var filesList = _getAllFilesFromFolder(path + "/data");
    var fileName = '';

    for(var i = 0; i < filesList.length; i++) {
        fileName = filesList[i].split('/');
        fileName = fileName.pop().split('.');
        dataJSON['$'+fileName[0]] = grunt.file.readJSON(filesList[i]);
    }

    // Load Grunt tasks declared in the package.json file
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);


    // Configure Grunt
    grunt.initConfig({

        // grunt-express will serve the files from the folders listed in `bases`
        // on specified `port` and `hostname`

        //sass: {
        //   dist: {
        //        expand: true,
        //        cwd: "test/",
        //        src: ["**/*.scss"],
        //        dest: "test/",
        //        ext: ".css"
        //    }
        //},

        // grunt-watch will monitor the projects files
        watch: {
            options: {
                livereload: true,
                spawn: false,
                interrupt: true
            },
            js: {
                options: {
                    nospawn: true,
                    interrupt: true
                },
                files: [path+'/blocks/**/*.js',path+'/resources/plugins/**/*.js'],
                tasks: ['concat:js']
            },
            jade: {
                options: {
                    nospawn: true,
                    interrupt: true
                },
                files: [path+'/blocks/**/*.jade',path+'/pages/**/*.jade'],
                tasks: ['jade']
            },
            json: {
                options: {
                    nospawn: true,
                    interrupt: true
                },
                files: [path+'/data/**/*.json'],
                tasks: ['prepareDataJSON', 'jade']
            },
            /*scss: {
                options: {
                    nospawn: true,
                    interrupt: true
                },
                files: [path+'/blocks/!**!/!*.scss',path+'/styles/!**!/!*.scss'],
                tasks: ['scss_compile']
            }*/
            stylus: {
                options: {
                    nospawn: true,
                    interrupt: true
                },
                files: [path+'/blocks/**/*.styl',path+'/styles/**/*.styl'],
                tasks: ['stylus_compile', 'path_replace']
            },
            stylus_bitrix: {
                options: {
                    nospawn: true,
                    interrupt: true
                },
                files: ['local/templates/**/*.styl','local/components/**/*.styl'],
                tasks: ['stylus:prod', 'path_replace']
            }
        },

        // grunt-open will open your browser at the project's URL
        open: {
            all: {
                // Gets the port from the connect configuration
                path: 'http://kruzenshtern.local/source/build/'
            }
        },

        /*
         "dr-svg-sprites": {
         tv: {
         options: {
         spriteElementPath: "local/templates/mobile/images",
         spritePath: "local/templates/mobile/sprites/dr-logos-tv-sprite.svg",
         cssPath: "local/templates/mobile/sprites/dr-logos-tv-sprite.css"
         }
         }
         }*/

        grunticon: {
            myIcons: {
                files: [{
                    expand: true,
                    cwd: path+'resources/images',
                    src: ['/svg-sprites/**/*.svg'],
                    dest: path+""
                }],
                options: {
                }
            }
        },

        uglify: {
            options: {
                sourceMap: false
            },
            build: {
                files: grunt.file.expandMapping(['local/templates/keypoint_tpl/**/*.js', '!local/templates/keypoint_tpl/**/*.min.js' , '!local/templates/keypoint_tpl/**/plugins/*.js'], '', {
                    rename: function(destBase, destPath) {
                        return destBase+destPath.replace('.js', '.min.js');
                        /*
                         var expr = new RegExp('.min.js', 'ig');
                         if (destBase+destPath.search(expr) != -1) {
                         return destBase+destPath.replace('.js', '.min.js');
                         } else {
                         return destBase+destPath.replace('.js', '.min.js');
                         }*/
                    }
                })
            }
        },

        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: path+'images/',
                        src: ['**/*.png'],
                        // Could also match cwd line above. i.e. project-directory/img/
                        dest: path+'images_optimized/',
                        ext: '.png'
                    },
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: 'upload/',
                        src: ['**/*.png'],
                        // Could also match cwd line above. i.e. project-directory/img/
                        dest: 'upload/',
                        ext: '.png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: path+'images/',
                        src: ['**/*.jpg'],
                        // Could also match cwd. i.e. project-directory/img/
                        dest: path+'images_optimized/',
                        ext: '.jpg'
                    },
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: 'upload/',
                        src: ['**/*.jpg'],
                        // Could also match cwd. i.e. project-directory/img/
                        dest: 'upload/',
                        ext: '.jpg'
                    }
                ]
            }
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'local/templates/keypoint_tpl/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'local/templates/keypoint_tpl/css/',
                    ext: '.min.css'
                }]
            }
        },

        svg_sprite: {
            basic: {
                // Target basics
                expand: true,
                cwd: path+'/resources/images',
                src: ['svg-sprites/**/*.svg'],
                dest: path+'/styles/helpers',
                // Target options
                options             : {
                    mode            : {
                        css         : {     // Activate the «css» mode
                            render  : {
                                styl : true  // Activate CSS output (with default options)
                            },
                            sprite : '../../resources/images/sprite.svg',
                            dest: ''

                        }
                    },
                    shape               : {
                        spacing         : {         // Add padding
                            padding     : 0
                        }

                    }
                }
            }
        },
/*        sprite:{
            all: {
                cwd: path+'resources/images',
                src: path+'/png-sprites/!**!/!*.png',
                dest: path+'result.png',
                destCss: path+'result.css',
                algorithm: 'alt-diagonal'
            }
        },*/
        jade: {
            compile: {
                options: {
                    pretty: true,
                    data: dataJSON
                },
                files: [{
                    cwd: path+"/pages",
                    src: "**/*.jade",
                    dest: path_build,
                    expand: true,
                    ext: ".html"
                }]
            }
        },
        stylus: {
            dev: {
                files: [{
                    'source/build/style.css': path + "/styles/main.styl"
                },
                    {'source/build/blocks.css': path + "/blocks/**/*.styl"}
                ]
            },
            prod: {
                options: {
                    paths: [path],
                    relativeDest: ''
                },
                files: [{
                    cwd: "local/templates",
                    dest: "local/templates",
                    src: "**/*.styl",
                    expand: true,
                    ext: ".css"
                },{
                    cwd: "local/components",
                    dest: "local/components",
                    src: "**/*.styl",
                    expand: true,
                    ext: ".css"
                }]
            }
        },
        sass: {
            options: {
                sourceMap: false
            },
            dist: {
                files: [{
                    'source/build/style.css': path+"/styles/main.scss"
                },
                    {'source/build/blocks.css': path+"/blocks/**/*.scss"}
                ]
            }
        },
        sprite:{
            all: {
                src: path+'/resources/images/png-sprites/*.png',
                dest: path+'/resources/images/sprite-sheet.png',
                destCss: path+'/styles/helpers/png-sprites.styl',
                cssVarMap: function (sprite) {
                    sprite.name = 'icon-' + sprite.name;
                },
                algorithm: 'top-down',
                imgPath: '/'+path_build+'/assets/images/sprite-sheet.png'
            }

        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {expand: true, flatten: true, src: [path + '/resources/images/*'], dest: path_build + '/assets/images', filter: 'isFile'}
                ]
            }
        },
        concat: {
            options: {
                separator: ';',
            },
            js: {
                src: [path+'/blocks/**/*.js'],
                dest: path_build+'/main.js',
            },
        },
        'string-replace': {
            all: {
                files: [{
                    expand: true,
                    cwd: path_build,
                    src: ['*.css'],
                    dest: path_build
                },
                    {
                        expand: true,
                        cwd: 'local/templates',
                        src: ['**/*.css'],
                        dest: 'local/templates'
                    }],
                options: {
                    replacements: [{
                        pattern: /\.\.\/\.\.\/resources\/images/ig,
                        replacement: '/source/build/assets/images'
                    }]
                }
            }
        }
    });

    //grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-dr-svg-sprites');
    grunt.loadNpmTasks('grunt-grunticon');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-svg-sprite');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    // Load in `grunt-spritesmith`
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-string-replace');


    // Creates the `server` task
    grunt.registerTask('server', [
        'open',
        'watch'
    ]);

    // Объединение SVG, преобразование в BASE64 Изображений и создание PNG
    grunt.registerTask('combine_svg', [
        'grunticon'
    ]);

    // Объеденить SVG в файл и преобразовать пути
    grunt.registerTask('combine_svg_to_file', [
        'svg_sprite', 'stylus', 'copy', 'path_replace'
    ]);

    // Минификация JS
    grunt.registerTask('minify', [
        'uglify'
    ]);

    // Оптимизация изображений
    grunt.registerTask('imageminjpg', ['imagemin:jpg']);
    grunt.registerTask('imageminpng', ['imagemin:png']);

    // Минификация CSS
    grunt.registerTask('cssminify', ['cssmin']);

    // Объединение PNG
    grunt.registerTask('combine_png', ['sprite', 'copy']);


    // Компиляция Jade
    grunt.registerTask('jade_compile', ['jade']);

    // Компиляция Stylus
    grunt.registerTask('stylus_compile', ['stylus:dev']);

    // Подготовка спрайтов в stylus
    //grunt.registerTask('stylus-sprite', ['sprite','copy']);

    // Компиляция SCSS
    grunt.registerTask('scss_compile', ['sass']);

    // Замена путей в файле
    grunt.registerTask('path_replace', ['string-replace']);

    // Сборка тестовых JSON Данных
    grunt.registerTask('prepareDataJSON', function() {
        var filesList = _getAllFilesFromFolder(path + "/data");
        var fileName = '';

        for(var i = 0; i < filesList.length; i++) {
            fileName = filesList[i].split('/');
            fileName = fileName.pop().split('.');
            dataJSON['$'+fileName[0]] = grunt.file.readJSON(filesList[i]);
        }
    });

    // Сборка проекта
    grunt.registerTask('build', ['jade','stylus', 'copy', 'path_replace', 'concat:js']);


};