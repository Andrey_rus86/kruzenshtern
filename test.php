<?

$path = 'photos';

$dirs = scandir($path);

foreach ($dirs as $dirKey => $dir) {
    if (is_dir($path . '/' . $dir)) {
        if ($dir == '.' || $dir == '..') continue;

        $files = scandir($path . '/' . $dir);

        foreach ($files as $file_name) {
            if ($file_name == '.' || $file_name == '..') continue;


// файл и новый размер
            $filename = $file_name;
            $percent = 0.5;
            $imagePath = $path.'/'.$dir.'/'.$filename;

// получение нового размера

            list($width, $height) = getimagesize($imagePath);
            $newwidth = $width * $percent;
            $newheight = $height * $percent;

// загрузка
            $thumb = imagecreatetruecolor($newwidth, $newheight);
            $source = imagecreatefromjpeg($imagePath);

// изменение размера
            imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

// вывод
            mkdir($path . '/' . $dir . '/thubms_medium/');
            imagejpeg($thumb, $path . '/' . $dir . '/thubms_medium/' . $file_name, 100);


            $percent = 0.2;

// получение нового размера
            list($width, $height) = getimagesize($imagePath);
            $newwidth = $width * $percent;
            $newheight = $height * $percent;

// загрузка
            $thumb = imagecreatetruecolor($newwidth, $newheight);
            $source = imagecreatefromjpeg($imagePath);

// изменение размера
            imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

// вывод
            mkdir($path . '/' . $dir . '/thubms_small/');
            imagejpeg($thumb, $path . '/' . $dir . '/thubms_small/' . $file_name, 100);
        }

    }
}


?>