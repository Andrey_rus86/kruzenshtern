<?php

/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 07.09.2016
 * Time: 11:21
 */

namespace Keypoint\BarqueCoordinates\Parsers;

use Keypoint\BarqueCoordinates\Parser;
use Keypoint\BarqueCoordinates\ParserInterface;

class ParserMarineTraffic extends Parser implements ParserInterface
{

    public function parseCoordinates() {

        $html = $this->grabPage('http://www.marinetraffic.com/ru/ais/index/positions/all/shipid:345607/mmsi:273243700/shipname:KRUZENSHTERN');

        preg_match_all('#<tr>.*?<td>.*?<time.*?>(.*?)</time>.*?</td>.*?<td>.*?</td>.*?<td>.*?</td>.*?<td>(.*?)</td>.*?<td>(.*?)</td>.*?</tr>#ims', $html, $matches);

        return [
          'DATE' => date('d.m.Y H:i',strtotime($matches[1][0])),
          'LATITUDE' => trim($matches[2][0]),
          'LONGITUDE' => trim($matches[3][0])
        ];
    }
}