<?php

namespace Keypoint\BarqueCoordinates;

interface ParserInterface
{
    public function parseCoordinates();
}