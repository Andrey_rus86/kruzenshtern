<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 07.09.2016
 * Time: 14:39
 */
$MESS['KRUZENSHTERN_COORDS'] = "Координаты: ";
$MESS['KRUZENSHTERN_COORDS_TITLE'] = "Где сейчас находится барк";
$MESS['KRUZENSHTERN_COORDS_TEXT'] = "Актуальные данные местонахождения барка «Крузенштерн» Вы можете отслеживать на интерактивной карте.";
$MESS['KRUZENSHTERN_COORDS_DATE_UPDATE'] = "Последнее обновление данных";