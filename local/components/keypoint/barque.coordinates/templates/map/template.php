<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 07.09.2016
 * Time: 11:12
 */
use Bitrix\Main\Localization\Loc;

$this->addExternalJs('http://maps.googleapis.com/maps/api/js?sensor=false');
$this->addExternalJs(LAYOUT_PATH . 'blocks/location/location.js');

$coords = Loc::getMessage('KRUZENSHTERN_COORDS').' '.number_format($arResult['COORDINATES']['LATITUDE'],4,'.','').'; '.number_format($arResult['COORDINATES']['LONGITUDE'],4,'.','');
$coords_date = Loc::getMessage('KRUZENSHTERN_COORDS_DATE_UPDATE').': '.$arResult['COORDINATES']['DATE'];

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/location-events/location-events.jade',
    [
        'location' => [
            'TITLE' => Loc::getMessage('KRUZENSHTERN_COORDS_TITLE'),
            'TEXT' => Loc::getMessage('KRUZENSHTERN_COORDS_TEXT'),
            'COORDINATES' => $coords,
            'COORDINATES_JSON' => json_encode([$arResult['COORDINATES']['LATITUDE'], $arResult['COORDINATES']['LONGITUDE']], JSON_UNESCAPED_UNICODE),
            'DATE' => $coords_date,
        ]
    ]);