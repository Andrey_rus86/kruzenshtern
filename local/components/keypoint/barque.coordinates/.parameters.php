<?
use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$parsersList = array_values(array_diff(scandir(__DIR__.'/class/parsers'), array('.', '..')));

$parsersList = array_map(function($elem) {
    return mb_substr($elem,0,-4);
},$parsersList);

$parsersList = array_combine($parsersList, $parsersList);

$arComponentParameters = array(
    "PARAMETERS" => array(
        "COORDINATES_PARSER" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::GetMessage("KRUZENSHTERN_COORDINATES_PARSER_DESC"),
            "TYPE" => "LIST",
            "VALUES" => $parsersList
        ),
        "CACHE_TIME" => array("DEFAULT" => 36000),
    ),
);
?>