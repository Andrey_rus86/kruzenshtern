<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("KRUZENSHTERN_COORDINATES_NAME"),
	"DESCRIPTION" => GetMessage("KRUZENSHTERN_COORDINATES_NAME"),
	"ICON" => "/images/catalog.gif",
	"COMPLEX" => "N",
	"SORT" => 10,
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "catalog",
			"NAME" => GetMessage("KRUZENSHTERN_COORDINATES_NAME"),
			"SORT" => 30,
		)
	)
);
?>