<?
use Bitrix\Main\Localization\Loc;
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mfeedback">
<?
$arInfo = [];
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	$arInfo['OK_MESSAGE'] = Loc::GetMessage("OK_MESSAGE");
}
?>
<?

$arInfo['TITLE'] = Loc::getMessage('KRUZENSHTERN_CONTATCS_TITLE');
$sessId = preg_match('#value="(.*?)"#ims',bitrix_sessid_post(), $mathces);
$arInfo['BITRIX_SESSID_POST'] = $mathces[1];
$arInfo['FORM_NAME'] = Loc::GetMessage("MFT_NAME");
$arInfo['FORM_EMAIL'] = Loc::GetMessage("MFT_EMAIL");
$arInfo['FORM_MESSAGE'] = Loc::GetMessage("MFT_MESSAGE");
$arInfo['CAPTCHA_SID'] = $arResult["capCode"];
$arInfo['USE_CAPTCHA'] = $arParams["USE_CAPTCHA"];
$arInfo["SRC_CAPTCHA"] = '/bitrix/tools/captcha.php?captcha_sid='.$arResult["capCode"];
$arInfo["PARAMS_HASH"] = $arResult["PARAMS_HASH"];
$arInfo["MFT_SUBMIT"] = Loc::GetMessage("MFT_SUBMIT");
$arInfo["MFT_CAPTCHA_PLACEHOLDER"] = Loc::GetMessage("MFT_CAPTCHA_PLACEHOLDER");
$arInfo["ERRORS"] = $arResult["ERROR_MESSAGE"];
$arInfo["AUTHOR_NAME"] = $arResult["AUTHOR_NAME"];
$arInfo["AUTHOR_EMAIL"] = $arResult["AUTHOR_EMAIL"];
$arInfo["AUTHOR_MESSAGE"] = $arResult["MESSAGE"];


foreach($arResult['SECTIONS'] as $section) {
	$arInfo['PERIOD'][] = [
		'CODE' => $section['CODE'],
		'NAME' => $section['NAME']
	];
}


echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/send-message/send-message.jade', [
		'sendmessage' => $arInfo
	]
);
?>
</div>
