<?
$MESS ['KRUZENSHTERN_CONTATCS_TITLE'] = "Отправить сообщение";
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_EMAIL'] = "Ваш E-mail";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_CAPTCHA_PLACEHOLDER'] = "Введите проверочный код";
$MESS ['MFT_SUBMIT'] = "Отправить";
$MESS ['OK_MESSAGE'] = "Ваше сообщение доставлено. Как правило, время ответа составляет до 1-2-х рабочих дней.";
?>