/**
 * Created by Андрей on 18.10.2016.
 */
$(document).ready(function() {
    //setFormHandler();
});

function setFormHandler() {
    // Отправка AJAX запроса формы
    $('.send-message__form').on('submit', function(evt) {
        evt.preventDefault();

        var formData = new FormData($(this)[0]);

        myUtils.sendAjax(formData)('/ajax/send-message/ajax.php', {insertNode: '#contactsFormContainer'});
    });
}