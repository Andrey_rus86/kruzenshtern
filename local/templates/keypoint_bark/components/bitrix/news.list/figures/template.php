<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
$this->addExternalJs(LAYOUT_PATH.'blocks/characteristics/characteristics.js');
?>
<?
$arInfo = [];

$arInfo['ITEMS'] = [];

foreach($arResult["ITEMS"] as $itemKey => $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));


	$arInfo['CIRCLES'][$arItem['CODE']] = [
		'NUMBER' => $arItem['DISPLAY_PROPERTIES']['NUMBER']['DISPLAY_VALUE'],
		'TEXT' => htmlspecialchars_decode($arItem['NAME']),
		'CLASS1' => 'circle__'.$arItem['CODE'],
		'CLASS2' => 'circle__'.$arItem['CODE'].'-number',
		'CLASS3' => 'circle__'.$arItem['CODE'].'-text'
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/figures/figures.jade', [
		'figures' => $arInfo
	]
);
?>
<?
// Статика
$APPLICATION->IncludeFile(
	"/include_areas/".LANGUAGE_ID."/barque/figures.php",
	Array(),
	Array("MODE"=>"html")
);
?>
