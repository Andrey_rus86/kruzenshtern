<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
$this->addExternalJs(LAYOUT_PATH.'blocks/inmotion/inmotion.js');
?>
<?
$arInfo = [];
$arInfo['TITLE'] = Loc::getMessage('KRUZENSHTERN_VIDEO_TITLE');
if(count($arResult["ITEMS"])> 1) {
	$arInfo['NEXT'] = Loc::getMessage('KRUZENSHTERN_VIDEO_NEXT');
	$arInfo['NEXT_NAME'] = $arResult["ITEMS"][1]['NAME'];
}
$arInfo['VIDEO'] = [];

foreach($arResult["ITEMS"] as $itemKey => $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$nextKey = $itemKey+1;
	if($nextKey>=count($arResult["ITEMS"])) $nextKey = 0;

	$arInfo['VIDEO'][] = [
		'FILE' => $arItem['DISPLAY_PROPERTIES']['FILE']['FILE_VALUE']['SRC'],
		'VIDEO_TITLE' => $arItem['NAME'],
		'NEXT_NAME' => $arResult["ITEMS"][$nextKey]['NAME']
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/inmotion/inmotion.jade', [
		'inmotion' => $arInfo
	]
);
?>
