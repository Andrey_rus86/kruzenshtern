<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
$this->addExternalJs(LAYOUT_PATH.'blocks/partners/partners.js');
?>
<?
$arInfo = [];
$arInfo['TITLE'] = Loc::getMessage('KRUZENSHTERN_PARTNERS_TITLE');
$arInfo['SLIDES'] = [];

foreach($arResult["ITEMS"] as $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$arInfo['SLIDES'][] = [
		'IMAGE' => $arItem['PREVIEW_PICTURE']['SRC'],
		'LINK' => !empty($arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']) ? $arItem['DISPLAY_PROPERTIES']['LINK']['VALUE'] : '#this',
		'ITEM_ID' => $this->GetEditAreaId($arItem['ID'])
	];
}


echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/partners/partners.jade', [
		'partners' => $arInfo
	]
);
?>
