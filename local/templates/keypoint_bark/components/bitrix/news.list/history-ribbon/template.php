<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
$this->addExternalJs(LAYOUT_PATH . 'blocks/history-slider/history-slider.js');
$this->addExternalJs(LAYOUT_PATH . '../build/assets/plugins/touchSwipe/jquery.touchSwipe.js');
?>
<?
$arInfo = [];
$arInfo['TITLE'] = Loc::getMessage('KRUZENSHTERN_HISTORY_SLIDER_TITLE');
$arInfo['INFO'] = Loc::getMessage('KRUZENSHTERN_HISTORY_SLIDER_INFO');
$arInfo['DATE_LINE_SOURCE'] = [];
$arInfo['DATE_LINE_EVENTS'] = [];
$arInfo['DATE_LINE'] = [];
$arInfo['SLIDES'] = [];

for ($i = 1924; (int)$i <= date('Y'); $i++) {
    $arInfo['DATE_LINE'][] = $i;
}


foreach ($arResult["ITEMS"] as $arItem) {
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

    $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 411, 'height' => 275), BX_RESIZE_IMAGE_EXACT, true);

    $arInfo['DATE_LINE_EVENTS'][] = (int)$arItem['DISPLAY_PROPERTIES']['YEAR']['VALUE'];
    $arInfo['SLIDES'][] = [
        'IMAGE' => $file['src'],
        'TITLE' => $arItem['NAME'],
        'YEARS' => (!empty($arItem['DISPLAY_PROPERTIES']['YEAR_STR']['VALUE'])) ? $arItem['DISPLAY_PROPERTIES']['YEAR_STR']['VALUE'] : $arItem['DISPLAY_PROPERTIES']['YEAR']['VALUE'],
        'TEXT' => $arItem['DISPLAY_PROPERTIES']['HISTORY_NOTE']['DISPLAY_VALUE'],
        'ITEM_ID' => $this->GetEditAreaId($arItem['ID']),
        'ID' => $arItem['ID']
    ];
}


$emptyDateCnt = 0;
$arInfo['DATE_LINE'] = array_map(function ($elem) use ($arInfo, &$emptyDateCnt) {
    if(!in_array($elem, $arInfo['DATE_LINE_EVENTS'])) {

        if($emptyDateCnt>3) {
            $elem = null;
        } else {
            $elem = substr($elem, -2);
            $emptyDateCnt++;
        }
    } else {
        $elem = "<span class='big-date'>".$elem."</span>";
        $emptyDateCnt = 0;
    }

    return $elem;

},
    $arInfo['DATE_LINE']);


$arInfo['DATE_LINE'] = array_unique($arInfo['DATE_LINE']);
if(($key = array_search(null, $arInfo['DATE_LINE'])) !== false) {
    unset($arInfo['DATE_LINE'][$key]);
}

$arInfo['DATE_LINE'] = array_values($arInfo['DATE_LINE']);

$emptyDateCnt = 0;
$elementsAdded = 0;
foreach($arInfo['DATE_LINE'] as $key => $value) {

    if(preg_match('#[0-9]{4}#ims', $value)) {

        if($emptyDateCnt<4) {
            $arrFill = array_fill(0,4-$emptyDateCnt,'...');

            $arInfo['DATE_LINE'] = array_merge(
                array_slice($arInfo['DATE_LINE'], 0, $key+$elementsAdded, true),
                $arrFill,
                array_slice($arInfo['DATE_LINE'], $key+$elementsAdded, count($arInfo['DATE_LINE']) - 1, true)
            );
            $elementsAdded += 4-$emptyDateCnt;
        }
        $emptyDateCnt = 0;
    } else {
        $emptyDateCnt++;
    }
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH . 'blocks/history-slider/history-slider.jade', [
        'historyslider' => $arInfo
    ]
);
?>
