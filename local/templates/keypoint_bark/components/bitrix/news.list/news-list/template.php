<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<?
$arInfo = [];

if($arParams["DISPLAY_BOTTOM_PAGER"] != true) {


	$arInfo['EVENTS_LINK'] = $arParams['PAGER_BASE_LINK'];
	if (preg_match('#past#ims', $arInfo['EVENTS_LINK'])) {
		$arInfo['LINK'] = Loc::getMessage('KRUZENSHTERN_ALL_PAST_EVENTS');
	} else {
		$arInfo['LINK'] = Loc::getMessage('KRUZENSHTERN_ALL_UPCOMING_EVENTS');
	}
}


foreach($arResult["ITEMS"] as $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>411, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);

	$arInfo['ITEM'][] = [
		'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'],
		'DATE' => $arItem['ACTIVE_FROM'],
		'IMAGE' => $file['src'],
		'TITLE' => html_entity_decode($arItem['NAME']),
		'TEXT' => html_entity_decode($arItem['PREVIEW_TEXT']),
		'ITEM_ID' => $this->GetEditAreaId($arItem['ID'])
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/events/events', [
		'events' => $arInfo
	]
);
?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br/>
	<div class="center-wrapper">
		<?=$arResult["NAV_STRING"]?>
	</div>
	<br/><br/>
<?endif;?>
