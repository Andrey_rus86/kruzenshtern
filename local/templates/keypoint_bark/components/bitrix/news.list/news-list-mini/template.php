<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<? /* Карта координат барка */ ?>
<? $coordsData = $APPLICATION->IncludeComponent(
	"keypoint:barque.coordinates",
	"",
	array(
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "Y",
		"COORDINATES_PARSER" => "ParserMarineTraffic"
	),
	$component
);?>
<?
$arInfo = [];


foreach($arResult["ITEMS"] as $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>411, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);

	$arInfo['EVENTS'][] = [
		'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'],
		'DATE' => $arItem['ACTIVE_FROM'],
		'TITLE' => html_entity_decode($arItem['NAME']),
		'TEXT' => !empty($arItem['DISPLAY_PROPERTIES']['SHORT_PREVIEW_TEXT']['VALUE']) ? html_entity_decode($arItem['DISPLAY_PROPERTIES']['SHORT_PREVIEW_TEXT']['VALUE']) : html_entity_decode($arItem['PREVIEW_TEXT']),
		'ITEM_ID' => $this->GetEditAreaId($arItem['ID'])
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/location/location.jade',
	[
		'location' => [
			'TITLE' => Loc::getMessage('KRUZENSHTERN_COORDS_TITLE'),
			'TEXT' => Loc::getMessage('KRUZENSHTERN_COORDS_TEXT'),
			'COORDINATES_JSON' => json_encode([$coordsData['COORDINATES']['LATITUDE'], $coordsData['COORDINATES']['LONGITUDE']], JSON_UNESCAPED_UNICODE),
			'EVENTSTITLE' => Loc::getMessage('KRUZENSHTERN_EVENTSTITLE'),
			'EVENTS' => $arInfo['EVENTS']
		]
	]);
?>
