<?
$MESS["KRUZENSHTERN_CHARACTERISTICS_TITLE"] = "Технические характеристики";
$MESS["KRUZENSHTERN_CHARACTERISTICS_CAPTION"] = "Наведите курсором на элемент, чтобы получить о нем информацию";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_0_0"] = "Длина габаритная: 114,5 м";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_0_1"] = "Ширина габаритная: 14,04 м";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_0_2"] = "Осадка по летнюю ватерлинию: 6,27 м";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_0_3"] = "Площадь парусов: 3400 м²";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_0_4"] = "Высота мачты над палубой 56 м";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_0_5"] = "Скорость под парусами: 17 узлов";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_1_0"] = "Скорость на двигателях: 10 узлов";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_1_1"] = "Количество палуб: 5";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_1_2"] = "Количество переборок: 7";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_1_3"] = "Экипаж: 60 человек";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_1_4"] = "Количество курсантов: 120 человек";
$MESS["KRUZENSHTERN_CHARACTERISTICS_ABOUT_1_5"] = "";

?>