<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
$this->addExternalJs(LAYOUT_PATH.'blocks/characteristics/characteristics.js');
?>
<?
$arInfo = [];
$arInfo['TITLE'] = Loc::getMessage('KRUZENSHTERN_CHARACTERISTICS_TITLE');
$arInfo['CAPTION'] = Loc::getMessage('KRUZENSHTERN_CHARACTERISTICS_CAPTION');


for($index = 0; $index<=1;$index++) {
	for ($i = 0; $i <= 5; $i++) {
		if (Loc::getMessage('KRUZENSHTERN_CHARACTERISTICS_ABOUT_'.$index.'_' . $i) != '') {
			$arInfo['ABOUT'][$index][] = Loc::getMessage('KRUZENSHTERN_CHARACTERISTICS_ABOUT_'.$index.'_' . $i);
		}
	}
}


$arInfo['ITEMS'] = [];

foreach($arResult["ITEMS"] as $itemKey => $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>152, 'height'=>152), BX_RESIZE_IMAGE_EXACT, true);
	$arInfo['ITEMS'][] = [
		'IMAGE' => !empty($file['src']) ? $file['src'] : '',
		'DETAIL_NAME' => $arItem['NAME'],
		'DETAIL_TEXT' =>$arItem['PREVIEW_TEXT'],
		'POS_X' => $arItem['DISPLAY_PROPERTIES']['POS_X']['VALUE'],
		'POS_Y' => $arItem['DISPLAY_PROPERTIES']['POS_Y']['VALUE'],
		'PROPS' => [$arItem['DISPLAY_PROPERTIES']['SHORT_NAME']['NAME'].': '.$arItem['DISPLAY_PROPERTIES']['SHORT_NAME']['VALUE'],
			$arItem['DISPLAY_PROPERTIES']['SIZES']['NAME'].': '.$arItem['DISPLAY_PROPERTIES']['SIZES']['VALUE'],
			$arItem['DISPLAY_PROPERTIES']['SQUARE']['NAME'].': '.$arItem['DISPLAY_PROPERTIES']['SQUARE']['VALUE']
		]

	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/characteristics/characteristics.jade', [
		'characteristics' => $arInfo,
		'characteristicsJSON' => json_encode($arInfo, JSON_UNESCAPED_UNICODE)
	]
);
?>
