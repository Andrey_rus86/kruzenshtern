<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);

$this->addExternalJs(LAYOUT_PATH.'../build/assets/plugins/fancyBox/jquery.fancybox.js');
$this->addExternalCss(LAYOUT_PATH.'../build/assets/plugins/fancyBox/jquery.fancybox.css');

$this->addExternalJs(LAYOUT_PATH.'/blocks/inner/inner.js')
?>
<?
$arInfo = [];

$arInfo['TITLE'] = Loc::getMessage('KRUZENSHTERN_INNER_TITLE');
$arInfo['INFO'] = Loc::getMessage('KRUZENSHTERN_INNER_INFO');
$arInfo['VECTOR_MAP'] = '/source/build/assets/images/ship_scheme.svg';


foreach($arResult["ITEMS"] as $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>138, 'height'=>172), BX_RESIZE_IMAGE_EXACT, true);
	$fileDetail = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>880, 'height'=>300), BX_RESIZE_IMAGE_EXACT, true);

	if($fileDetail==false) {
		$fileDetail['src'] = $arItem['DETAIL_PICTURE']['SRC'];
	}

	$arInfo['ROOMS'][] = [
		'CODE' => $arItem['CODE'],
		'IMAGE' => $file['src'],
		'DETAIL_IMAGE' => $fileDetail['src'],
		'NAME' => $arItem['NAME'],
		'DESCRIPTION' => $arItem["DETAIL_TEXT"]."<br/><br/>"."<a href='".$arItem['DETAIL_PICTURE']['SRC']."' rel='fancybox' class='fancyboxElem'><img src='".$fileDetail['src']."' class='inner__detailPicture' /></a>",
		'ITEM_ID' => $this->GetEditAreaId($arItem['ID'])
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/inner/inner', [
		'inner' => $arInfo
	]
);
?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br/>
	<div class="center-wrapper">
		<?=$arResult["NAV_STRING"]?>
	</div>
	<br/><br/>
<?endif;?>
