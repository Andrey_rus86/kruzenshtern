<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<?
$arInfo = [];

$arInfo['ITEMS'] = [];

foreach($arResult["ITEMS"] as $itemKey => $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$arInfo['ITEMS'][] = [
		'NAME' => $arItem['NAME'],
		'PICTURE' => $arItem['PREVIEW_PICTURE']['SRC']
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/sails/sails.jade', [
		'sails' => $arInfo
	]
);
?>
