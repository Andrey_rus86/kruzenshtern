<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
$this->addExternalJs(LAYOUT_PATH.'blocks/history/history.js');
?>
<?
$arInfo = [];
$arInfo['TITLE'] = Loc::getMessage('KRUZENSHTERN_SLIDER_TITLE');
$arInfo['LINK'] = Loc::getMessage('KRUZENSHTERN_SLIDER_LINK');
$arInfo['SECTION_LINK'] = $arResult['SECTION']['PATH'][0]['LIST_PAGE_URL'];
$arInfo['SLIDES'] = [];
$arInfo['INFO'] = [];

foreach($arResult["ITEMS"] as $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$file = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>411, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);
	$arInfo['SLIDES'][] = $file['src'];
	$arInfo['INFO'][] = [
		'DATE' => $arItem["DISPLAY_PROPERTIES"]['YEAR']['DISPLAY_VALUE'],
		'TITLESMALL' => $arItem['NAME'],
		'TXT' => $arItem['PREVIEW_TEXT'],
		'ITEM_ID' => $this->GetEditAreaId($arItem['ID'])
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/history/history.jade', [
		'historyJSON' => json_encode($arInfo, JSON_UNESCAPED_UNICODE),
		'history' => $arInfo
	]
);
?>
