<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);

?>
<?
$arInfo = [];


foreach($arResult["ITEMS"] as $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$file = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>200, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);

	$arInfo['ITEMS'][] = [
		'FULL_IMG' => $arItem['DETAIL_PICTURE']['SRC'],
		'IMG' => $file['src'],
		'TITLE' => html_entity_decode($arItem['NAME']),
		'ITEM_ID' => $this->GetEditAreaId($arItem['ID'])
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/photo-album/photo-album', [
		'album' => $arInfo
	]
);
?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br/>
	<div class="center-wrapper">
		<?=$arResult["NAV_STRING"]?>
	</div>
	<br/><br/>
<?endif;?>
