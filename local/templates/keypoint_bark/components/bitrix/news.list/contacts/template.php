<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
$arInfo = [];
?>
<?
ob_start();
$APPLICATION->IncludeFile(
	"/include_areas/".LANGUAGE_ID."/contacts/intro.php",
	Array(),
	Array("MODE"=>"html", "NAME" => 'Заголовок текста приветствия')
);
$arInfo['TEXT'] = ob_get_clean();
ob_start();
$APPLICATION->IncludeFile(
	"/include_areas/".LANGUAGE_ID."/contacts/title.php",
	Array(),
	Array("MODE"=>"html", "NAME" => 'Блок текста приветствия')
);
$arInfo['TITLE'] = ob_get_clean();
?>
<?

foreach($arResult["ITEMS"] as $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$arInfo['ITEMS'][] = [
		'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'],
		'DATE' => $arItem['ACTIVE_FROM'],
		'TITLE' => html_entity_decode($arItem['NAME']),
		'ADDRESS' => $arItem['DISPLAY_PROPERTIES']['ADDRESS']['VALUE'],
		'FIO' => $arItem['DISPLAY_PROPERTIES']['FIO']['DISPLAY_VALUE'],
		'TEL' => !empty($arItem['DISPLAY_PROPERTIES']['TEL']['VALUE']) ? Loc::getMessage('KRUZENSHTERN_CONTACTS_TEL').' '.$arItem['DISPLAY_PROPERTIES']['TEL']['VALUE'] : '',
		'FAX' => !empty($arItem['DISPLAY_PROPERTIES']['FAX']['VALUE']) ? Loc::getMessage('KRUZENSHTERN_CONTACTS_FAX').' '.$arItem['DISPLAY_PROPERTIES']['FAX']['VALUE'] : '',
		'MOBILE' => !empty($arItem['DISPLAY_PROPERTIES']['MOBILE']['VALUE']) ? Loc::getMessage('KRUZENSHTERN_CONTACTS_MOBILE').' '.$arItem['DISPLAY_PROPERTIES']['MOBILE']['VALUE'] : '',
		'EMAIL' => $arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'],
		'EMAIL_LINK' => 'mailto:'.$arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'],
		'WEB' => $arItem['DISPLAY_PROPERTIES']['WEB']['VALUE'],
		'WEB_LINK' => 'http://'.$arItem['DISPLAY_PROPERTIES']['WEB']['VALUE'],
		'ADDITIONAL' => $arItem['PREVIEW_TEXT'],
		'ITEM_ID' => $this->GetEditAreaId($arItem['ID'])
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/contacts/contacts', [
		'contacts' => $arInfo
	]
);
?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br/>
	<div class="center-wrapper">
		<?=$arResult["NAV_STRING"]?>
	</div>
	<br/><br/>
<?endif;?>
