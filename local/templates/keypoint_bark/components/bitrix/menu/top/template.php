<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):
	$arItems = [];

	foreach($arResult as $arItem) {
		$arItem['NAME'] = $arItem['TEXT'];
		$arItem['SELECTED'] = $arItem['SELECTED'];
		$arItems[] = $arItem;
	}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/top-menu/top-menu.jade', [
			'topmenu' => $arItems
		]
	);

	?>
<?endif?>