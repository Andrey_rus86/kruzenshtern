<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):
	$arItems = [];
	$newLine = false;
	$oneLevel = [];

	foreach($arResult as $arItem) {
		if($arItem['DEPTH_LEVEL'] == 1 && $newLine) {
			$arItems[] = $oneLevel;
			$oneLevel = [];

		}

		if($arItem['DEPTH_LEVEL'] == 1) {
			$newLine = true;
			$oneLevel['TITLE'] = $arItem['TEXT'];
			$oneLevel['LINK'] = $arItem['LINK'];
		} else if($arItem['DEPTH_LEVEL'] == 2) {
			$arItem['NAME'] = $arItem['TEXT'];
			$oneLevel[] = $arItem;
		}
	}

	if(!empty($oneLevel)) {
		$arItems[] = $oneLevel;
	}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/footer-menu/footer-menu.jade', [
			'footer' => ['MENU'=> $arItems]
		]
	);

	?>
<?endif?>