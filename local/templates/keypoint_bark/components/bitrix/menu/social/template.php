<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):
	$arItems = [];

	foreach($arResult as $arItem) {
		$arItem['NAME'] = $arItem['TEXT'];
		$arItem['CLASS'] = $arItem['PARAMS']['CLASS'];
		$arItems[] = $arItem;
	}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/social-menu/social-menu.jade', [
			'social' => $arItems
		]
	);

	?>
<?endif?>