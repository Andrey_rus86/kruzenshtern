<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);

$this->addExternalJs(LAYOUT_PATH.'blocks/period/period.js');

?>
<?
$arInfo = [];
$arInfo['TEXT'] = Loc::getMessage('KRUZENSHTERN_PERIOD_TEXT');
$arInfo['PERIOD'] = [];

foreach($arResult['SECTIONS'] as $section) {
	// todo: вынести в свойство или параметр признак директории по умолчанию
	$default = '';
	if($section['CODE'] == 'year2006') {
		$default = 'selected';
	}
	$arInfo['PERIOD'][] = [
		'CODE' => $section['CODE'],
		'SELECTED' => ($_REQUEST['SECTION_CODE'] == $section['CODE']) ? 'selected' : $default,
		'NAME' => $section['NAME']
	];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/period/period.jade', [
		'period' => $arInfo
	]
);
?>
