<?php
use Bitrix\Iblock\ElementTable;

$sectionsID = [];
foreach($arResult['SECTIONS'] as $section) {
    $sectionsID[] = $section['ID'];
}

// Выбираем элементы
$resPhotos = ElementTable::getList(
    [
        'select' => ['ID', 'NAME', 'PREVIEW_TEXT', 'IBLOCK_SECTION_ID', 'PREVIEW_PICTURE'],
        'order' => ['SORT'=>'ASC', 'ID'=>'ASC'],
        'filter' => ['IBLOCK_SECTION_ID' => $sectionsID, "IBLOCK_ID" => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
            'ACTIVE' => 'Y']
    ]
);

// Получаем свойства фотографии
$arFilter = Array(
    "IBLOCK_ID"=>Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
    "ACTIVE"=>"Y",
    'SECTION_ID' => $sectionsID,
    'INCLUDE_SUBSECTIONS' => 'Y'
);
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false,false, ['ID', 'PROPERTY_REAL_PICTURE', 'PROPERTY_IS_MOVIE', 'PROPERTY_VIDEO_SIZES']);
$photoElems = [];
while($ar_fields = $res->GetNext()) {

    $photoElems[$ar_fields['ID']][] = $ar_fields;
}

// Получаем описание свойств
$propertyMovie = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
    "CODE"=>"IS_MOVIE"));
$isMoviePropID = null;
while($enum_fields = $propertyMovie->GetNext())
{
    $isMoviePropID = $enum_fields["ID"];
}

while($dataPhoto = $resPhotos->fetch()) {
    foreach($arResult['SECTIONS'] as $key => $section) {
        if($section['ID'] == $dataPhoto['IBLOCK_SECTION_ID']) {
            $rsFile = CFile::GetByID($dataPhoto["PREVIEW_PICTURE"]);
            $arFile = $rsFile->Fetch();

            $file = CFile::ResizeImageGet($dataPhoto['PREVIEW_PICTURE'], array('width'=>420, 'height'=>306), BX_RESIZE_IMAGE_EXACT, true);

            $additionalClass = '';
            $fancyboxType = '';
            $imageFull = '/upload/'.$arFile['SUBDIR'].'/'.$arFile['FILE_NAME'];
            if($photoElems[$dataPhoto['ID']][0]['PROPERTY_IS_MOVIE_ENUM_ID'] == $isMoviePropID) {

                $additionalClass = 'movie';
                $fancyboxType = 'ajax';
                $movieFile = CFile::GetByID($photoElems[$dataPhoto['ID']][0]['PROPERTY_REAL_PICTURE_VALUE']);
                $movieFileInfo = $movieFile->fetch();
                $imageFull = '/ajax/video-popup/ajax.php?width='.$photoElems[$dataPhoto['ID']][0]['PROPERTY_VIDEO_SIZES_VALUE'].
                    '&height='.$photoElems[$dataPhoto['ID']][1]['PROPERTY_VIDEO_SIZES_VALUE'].
                    '&path='.urlencode('/upload/'.$movieFileInfo['SUBDIR'].'/'.$movieFileInfo['FILE_NAME']);
            }



            $arResult['SECTIONS'][$key]['ITEMS'][] = [
                'IMAGE_FULL' => $imageFull,
                'IMAGE' => $file['src'],
                'ADDITIONAL_CLASS' => $additionalClass,
                'FANCYBOX_TYPE' => $fancyboxType,
                'CAPTION' => $dataPhoto['PREVIEW_TEXT']
            ];
        }
    }
}