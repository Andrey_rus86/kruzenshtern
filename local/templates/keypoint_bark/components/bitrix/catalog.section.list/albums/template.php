<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);

$this->addExternalJs(LAYOUT_PATH.'../build/assets/plugins/fancyBox/jquery.fancybox.js');
$this->addExternalCss(LAYOUT_PATH.'../build/assets/plugins/fancyBox/jquery.fancybox.css');

$this->addExternalJs(LAYOUT_PATH.'blocks/gallery-slider/gallery-slider.js');
?>
<?
foreach($arResult['SECTIONS'] as $section) {

	$arInfo = [];
	$arInfo['TITLE'] = $section['NAME'];
	$arInfo['TEXT'] = $section['DESCRIPTION'];
	$arInfo['SLIDER'] = $section['ITEMS'];

	echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/history-period/history-period.jade', [
			'newhistory' => $arInfo
		]
	);

}

?>
