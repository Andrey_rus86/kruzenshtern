<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);

$file = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE'], array('width'=>138, 'height'=>172), BX_RESIZE_IMAGE_EXACT, true);
$fileDetail = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE'], array('width'=>880, 'height'=>300), BX_RESIZE_IMAGE_EXACT, true);
if($fileDetail==false) {
	$fileDetail['src'] = $arResult['DETAIL_PICTURE']['SRC'];
}
$arInfo['ROOMS'][0]['NAME'] = $arResult["NAME"];
$arInfo['ROOMS'][0]['IMAGE'] = $file['src'];
$arInfo['ROOMS'][0]['DESCRIPTION'] = $arResult["DETAIL_TEXT"]."<br/><br/>"."<a href='".$arResult['DETAIL_PICTURE']['SRC']."' rel='fancybox' class='fancyboxElem'><img src='".$fileDetail['src']."' class='inner__detailPicture' /></a>";
$arInfo['ROOMS'][0]['ADDITIONAL_PHOTOS'] = [];

foreach($arResult['DISPLAY_PROPERTIES']['ADDITIONAL_PHOTOS']['FILE_VALUE'] as $photo) {
	if(!is_array($photo)) {
		// Если в дополнительных фотографиях - один файл
		$photo = $arResult['DISPLAY_PROPERTIES']['ADDITIONAL_PHOTOS']['FILE_VALUE'];
		$file = CFile::ResizeImageGet($photo, array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_EXACT, true);
		$arInfo['ROOMS'][0]['ADDITIONAL_PHOTOS'][] = ['PREVIEW' => $file['src'], 'FULL' => $photo['SRC']];
		break;
	}
	$file = CFile::ResizeImageGet($photo, array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_EXACT, true);
	$arInfo['ROOMS'][0]['ADDITIONAL_PHOTOS'][] = ['PREVIEW' => $file['src'], 'FULL' => $photo['SRC']];
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/compartment/compartment.jade', [
		'inner' => $arInfo
	]
);
?>