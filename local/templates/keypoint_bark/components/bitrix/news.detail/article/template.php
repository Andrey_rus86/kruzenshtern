<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);

$this->addExternalJs(LAYOUT_PATH.'../build/assets/plugins/fancyBox/jquery.fancybox.js');
$this->addExternalCss(LAYOUT_PATH.'../build/assets/plugins/fancyBox/jquery.fancybox.css');

$this->addExternalJs(LAYOUT_PATH.'blocks/text-images/text-images.js');

$file = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width'=>420, 'height'=>320), BX_RESIZE_IMAGE_EXACT, true);

$arInfo['TITLE1'] = html_entity_decode($arResult["NAME"]);
$arInfo['DATE'] = $arResult["ACTIVE_FROM"];
$arInfo['IMAGE_FULL'] = $arResult['DETAIL_PICTURE']['SRC'];
$arInfo['IMAGE1'] = $file['src'];
$arInfo['TEXT1'] = preg_replace("/<div>(.*?)<\/div>/", "$1", $arResult['DETAIL_TEXT']);
$arInfo['LIST_PAGE_URL'] = ($arParams['IBLOCK_URL']!='') ? $arParams['IBLOCK_URL'] : $arResult['LIST_PAGE_URL'];
$arInfo['SHOW_LIST_PAGE_URL'] = Loc::getMessage('SHOW_LIST_PAGE_URL');
$arInfo['AUTHOR'] = $arResult["DISPLAY_PROPERTIES"]['AUTHOR']['DISPLAY_VALUE'];

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/text-images/text-images.jade', [
		'textimages' => $arInfo
	]
);

// Вывод фотографий
if(isset($arResult["DISPLAY_PROPERTIES"]['ALBUM']['VALUE'])) {

	?>
	<div class="center-wrapper">
		<?$APPLICATION->IncludeComponent("bitrix:news.list","photo-album",Array(
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "services",
				"IBLOCK_ID" => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
				"NEWS_COUNT" => "100",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => Array("PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE"),
				"PROPERTY_CODE" => Array("YEAR"),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "Y",
				"SET_META_KEYWORDS" => "Y",
				"SET_META_DESCRIPTION" => "Y",
				"SET_LAST_MODIFIED" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
				"PARENT_SECTION" => $arResult["DISPLAY_PROPERTIES"]['ALBUM']['VALUE'],
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "Y",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"PAGER_BASE_LINK_ENABLE" => "Y",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
				"MESSAGE_404" => "",
				"PAGER_BASE_LINK" => "/",
				"PAGER_PARAMS_NAME" => "arrPager",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
			), $component
		);?>
	</div>
	<?
}

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/list-page-link/list-page-link.jade', [
		'textimages' => $arInfo
	]
);
?>