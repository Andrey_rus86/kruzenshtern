<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$app = Application::getInstance();
$appContext = $app->getContext();
$layoutFolder = '/source/build/assets/';
$curDir = $APPLICATION->GetCurDir();

?>
<!DOCTYPE html>
<html>
<head>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico">
    <?
    Asset::getInstance()->addJs($layoutFolder . 'plugins/jquery/jquery-1.12.3.min.js');
    Asset::getInstance()->addJs($layoutFolder . 'plugins/JScrollPane/jquery.jscrollpane.js');
    Asset::getInstance()->addJs($layoutFolder . 'plugins/JScrollPane/jquery.mousewheel.js');
    Asset::getInstance()->addJs($layoutFolder . 'plugins/slick/slick.min.js');
    Asset::getInstance()->addJs($layoutFolder . 'plugins/svgjs/svg.min.js');
    Asset::getInstance()->addJs($layoutFolder . 'js/utils.js');

    Asset::getInstance()->addCss($layoutFolder . 'plugins/JScrollPane/jquery.jscrollpane.css');
    Asset::getInstance()->addCss($layoutFolder . 'plugins/slick/slick.css');
    Asset::getInstance()->addCss($layoutFolder . '../style.css');

    $APPLICATION->ShowHead();

    ?>
</head>

<body class="page">
<div id="panel"><?= $APPLICATION->ShowPanel() ?></div>
<div class="outer-wrapper">
    <div class="page-wrapper">
        <div class="header">
            <div class="header__top">
                <div class="center-wrapper">

                    <?
                    // HEADER
                    echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/top/top.jade',
                        [
                            'top' => [
                                'TEXT' => Loc::getMessage('KRUZENSHTERN_TOP_TITLE'),
                                'SITE_DIR' => SITE_DIR
                            ]
                        ]);
                    ?>


                </div>
            </div>

            <div class="header__top-menu">
                <div class="center-wrapper">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "top",
                        Array(
                            "ROOT_MENU_TYPE" => "top",
                            "MAX_LEVEL" => "1",
                            "USE_EXT" => "N",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => Array()
                        )
                    ); ?>
                </div>
                <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "inc",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "EDIT_TEMPLATE" => ""
                    )
                );?>



            </div>

        </div>
