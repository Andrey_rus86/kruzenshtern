<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
				?>

	<div class="footer">
		<div class="center-wrapper">
			<? $APPLICATION->IncludeComponent(
				"bitrix:menu",
				"bottom",
				Array(
					"ROOT_MENU_TYPE" => "bottom",
					"CHILD_MENU_TYPE" => "sub",
					"MAX_LEVEL" => "2",
					"USE_EXT" => "Y",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => Array()
				)
			); ?>

			<div class="footer__logo svg-svg-sprites--logo-small svg-svg-sprites--logo-small-dims"></div>

		</div>

		<div class="center-wrapper">
			<div class="footer__about">
				<? $APPLICATION->IncludeComponent(
					"bitrix:menu",
					"social",
					Array(
						"ROOT_MENU_TYPE" => "social",
						"MAX_LEVEL" => "1",
						"USE_EXT" => "N",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => Array()
					)
				); ?>
				<div class="footer__info">
					<a href="http://keypoint.ru" class="footer__keypoint">Сделано в
						<div class="svg-svg-sprites--keypoint_logo_orange_white svg-svg-sprites--keypoint_logo_orange_white-dims"></div>
					</a>
					<div class="footer__copyright">© 2016 Балтийская государственная академия, все права защищены</div>
				</div>
			</div>
		</div>

	</div>

	</div>
</div>

<div id="loading-light"></div>
<div id="mask80"></div>
<div id="mask"></div>

</body></html>