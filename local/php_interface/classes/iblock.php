<?php

namespace Keypoint\Utils;

use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Loader;

/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 07.04.2016
 * Time: 13:43
 */
class Iblock
{
    /**
     * Return Iblock ID by its CODE
     * @param $code
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getIblockIdByCode($code)
    {
        Loader::includeModule('iblock');

        // Cache data
        $obCache = Cache::createInstance();
        $cache_time = "86400";
        $cache_id = static::class . "_" . __FUNCTION__ . "_" . $code;

        if ($obCache->initCache($cache_time, $cache_id)) {
            $data = $obCache->GetVars();
        } elseif ($obCache->startDataCache()) {
            $res = IblockTable::getList([
                'filter' => ['CODE' => $code]
            ]);

            if ($data = $res->fetch()) {
                $obCache->endDataCache($data);
            } else {
                $obCache->abortDataCache();
            }
        }

        return $data['ID'];
    }

    /**
     * Получаем ID раздела по ее коду
     * @param $iblockId
     * @param $code
     */
    public function getSectionIdByCode($iblockId, $code) {
        $res = Sectiontable::getList([
            'select' => ['ID'],
            'filter' => ['IBLOCK_ID'=>$iblockId, 'CODE'=>$code]
        ]);

        while($section = $res->fetch()) {
            return $section['ID'];
        }

        return null;
    }


    public function OnAfterIBlockElementAddHandler($arFields) {

    }
}