<?php
use Tale\Jade;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

$appContext = Application::getInstance()->getContext();
$server = $appContext->getServer();

# Автозагрузчик composer
include_once ($server->getDocumentRoot() . '/local/php_interface/classes/vendor/autoload.php');

# Константы
include_once($server->getDocumentRoot() . '/local/php_interface/constants.php');

Loader::registerAutoLoadClasses(null, [
    'Keypoint\Utils\Iblock' => '/local/php_interface/classes/iblock.php',
]);


// JADE шаблонизатор
$GLOBALS['JADE_RENDERER'] = new Jade\Renderer([
    //'pretty' => true,
    'cache_path' => $server->getDocumentRoot().'/tale_jade_cache',
    'paths' => [$server->getDocumentRoot()]
]);
