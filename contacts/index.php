<?
use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Барк «Крузенштерн» контакты");
$APPLICATION->SetTitle("Барк «Крузенштерн» контакты");

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/contacts.css');
?>
<?$APPLICATION->IncludeComponent("bitrix:news.list","contacts",Array(
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "services",
        "IBLOCK_ID" => Keypoint\Utils\Iblock::getIblockIdByCode('contacts'),
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => Array("PREVIEW_PICTURE", "PREVIEW_TEXT"),
        "PROPERTY_CODE" => Array("ADDRESS", "FIO", "TEL", "FAX", "MOBILE", "EMAIL", "WEB"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "Y",
        "SET_BROWSER_TITLE" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y",
        "SET_LAST_MODIFIED" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "ru",
        "INCLUDE_SUBSECTIONS" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "SET_STATUS_404" => "Y",
        "SHOW_404" => "Y",
        "MESSAGE_404" => "",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => ""
    )
);?>

<?
ob_start();
$APPLICATION->IncludeFile(
    "/include_areas/".LANGUAGE_ID."/contacts/intro_middle.php",
    Array(),
    Array("MODE"=>"html")
);
$intro = ob_get_clean();
ob_start();
$APPLICATION->IncludeFile(
    "/include_areas/".LANGUAGE_ID."/contacts/title_middle.php",
    Array(),
    Array("MODE"=>"html")
);
$title = ob_get_clean();

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/information/information.jade',
    [
        'information' => [
            'TITLE' => $title,
            'TEXT' => $intro
        ]
    ]);

?>

<?$APPLICATION->IncludeComponent(
	"bitrix:map.google.view", 
	"contacts",
	array(
		"API_KEY" => "",
		"INIT_MAP_TYPE" => "ROADMAP",
		"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:54.73786525375614;s:10:\"google_lon\";d:20.507250442504866;s:12:\"google_scale\";i:14;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:105:\"Управление мореплавания и практической подготовки БГАРФ\";s:3:\"LON\";d:20.5144572258;s:3:\"LAT\";d:54.738806497854;}}}",
		"MAP_WIDTH" => "100%",
		"MAP_HEIGHT" => "637",
		"CONTROLS" => array(
			0 => "SMALL_ZOOM_CONTROL",
			1 => "TYPECONTROL",
			2 => "SCALELINE",
		),
		"OPTIONS" => array(
			0 => "ENABLE_DBLCLICK_ZOOM",
			1 => "ENABLE_DRAGGING",
			2 => "ENABLE_KEYBOARD",
		),
		"MAP_ID" => "gm_1",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<div id="contactsFormContainer">
<?$APPLICATION->IncludeComponent("bitrix:main.feedback","feedback",Array(
        "USE_CAPTCHA" => "Y",
        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
        "REQUIRED_FIELDS" => Array("NAME","EMAIL","MESSAGE"),
        "EVENT_MESSAGE_ID" => Array("5")
    )
);?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>