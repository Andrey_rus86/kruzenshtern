source

    app - директория разработки (то, что собирается сборщиком)
        blocks - каждый отдельный логический блок сайта содержит в себе разметку (Jade), стилистику (Stylus) и скрпты (JS)
        data - объекты с информацией, используемой в блоках (blocks). В названии файлов не должно быть символа тире "-"
        helpers - вспомогательные файлы Jade
        pages - страницы сайта
        resources
            images
                png-sprites - склад PNG файлов для генерации спрайта
                svg-sprites - склад SVG файлов для генерации спрайта
        styles - общие и вспомогательные файлы стилей



    build - директория сборки (то, что уже имеет итоговый, собранный вид)
        assets - файлы в корне генерируются автоматически (автосборка)
            fonts - хранение файлов шрифтов
            images - все изображения
            plugins - JS plugin`ы, сторонние скрипты



Для запуска проекта вызывается команда
grunt server

Сборка сайта
grunt build

Генерация png спрайта
grunt combine_png

Генерация svg спрайта
grunt combine_svg_to_file