$(document).ready(function() {
    var timer = null;
    $('.characteristics__dot').on('mouseover click', function(evt) {
        var index = $(this).data('index');
        $('.characteristics__detail').addClass('hide');
        timer = setTimeout(function() {
            if(dataCharacteristics.ITEMS[index].IMAGE != '') {
                $('.characteristics__detail-image').css('display','block');
                $('.characteristics__detail-image').attr('src', dataCharacteristics.ITEMS[index].IMAGE);
            } else {
                $('.characteristics__detail-image').css('display','none');
            }
            $('.characteristics__detail-name').html(dataCharacteristics.ITEMS[index].DETAIL_NAME);
            $('.characteristics__detail-text').html(dataCharacteristics.ITEMS[index].DETAIL_TEXT);


            for (var i = 0; i < dataCharacteristics.ITEMS[index].PROPS.length; ++i) {
                $('.characteristics__detail-text').append("<p class='detail-p'>"+dataCharacteristics.ITEMS[index].PROPS[i]+"</p>");
            }

            $('.characteristics__detail').removeClass('hide');
        }, 400);

    });
});