$(document).ready(function(){
    // Инициализация плагина Popup окна
    $(".fancybox").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'none',
        aspectRatio : true,
        autoWidth: true,
        autoHeight: true,
        afterShow: function () {

            var fancyboxContainer = $('.fancybox-opened video');
            if(fancyboxContainer.length>0) {
                $.fancybox.update();
            }

        }
    });
});

$(window).load(function() {
    initGallery();
});

function initGallery() {
    // Подгружаем изображения
    $('.gallery-slider__slider').each(function() {
       var $images = $(this).find('img');
        var $countImages = $images.length;
        var $loadedImages = 0;
        var $allLoaded = false;
        var $slider = $(this);
        $images.each(function() {
            /*$(this)[0].onload = function() {
                $loadedImages++;
                if($loadedImages == $countImages) {

                }
            };*/
            $(this).attr('src', $(this).data('src'));
        });
        initPlugins($slider);
    });

    function initPlugins(container) {
        $slickSlider = container;
        // Инициализвция плагина слайдера
        $slickSlider.slick({
            infinite: false,
            slidesToShow: 2.5,
            slidesToScroll: 1,
            swipe: false
        });

        // Инициализация плагина прокрутки
        $scrollPane = $slickSlider.find('.slick-list').jScrollPane();

        // Переписываем функционал поведения слайдера при нажатии на кнопки прокрутки
        $('.gallery-slider button.slick-next.slick-arrow').off('click');
        $('.gallery-slider button.slick-next.slick-arrow').on('click', function(event) {
            event.preventDefault();
            $parentSilder = $(this).parents('.gallery-slider__slider');
            var slideWidth = $parentSilder.find('.gallery-slider__slide').outerWidth(true);
            var scrollApi = $parentSilder.find('.jspScrollable').data('jsp');

            scrollApi.scrollBy(slideWidth, 0, true);
        });

        $('.gallery-slider button.slick-prev.slick-arrow').off('click');
        $('.gallery-slider button.slick-prev.slick-arrow').on('click', function(event) {
            event.preventDefault();
            $parentSilder = $(this).parents('.gallery-slider__slider');
            var slideWidth = $parentSilder.find('.gallery-slider__slide').outerWidth(true);
            var scrollApi = $parentSilder.find('.jspScrollable').data('jsp');

            scrollApi.scrollBy(-slideWidth, 0, true);

        });
    }


}