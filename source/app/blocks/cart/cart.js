$(document).ready(function() {
    var coord1 = new google.maps.LatLng(54.739401, 20.508661);
    var coord2 = new google.maps.LatLng(54.737598, 20.514967);
    var coord3 = new google.maps.LatLng(54.738639, 20.514903);

    var MY_MAPTYPE_ID = 'custom_style';

    function initialize() {
        try {
            var featureOpts = [];

            var mapProp = {
                center: coord1,
                zoom: 14,
                scrollwheel: false,
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
                },
                mapTypeId: MY_MAPTYPE_ID
            };
            var map = new google.maps.Map(document.getElementById("cartMap"), mapProp);

            var styledMapOptions = {
                name: 'Мы на карте'
            };
            var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

            map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

            var image = {
                //url: '/source/build/assets/images/marker.png'
                //scaledSize: new google.maps.Size(46, 65)
            };

            var marker1 = new google.maps.Marker({
                position: coord1,
                map: map,
                icon: image
            });

            var marker2 = new google.maps.Marker({
                position: coord2,
                map: map,
                icon: image
            });

            var marker3 = new google.maps.Marker({
                position: coord3,
                map: map,
                icon: image
            });
        } catch (err) {

        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);

});
