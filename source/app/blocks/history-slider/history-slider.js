$(document).ready(function () {
    // Плагин добавления swipe функционала для блока
    var prevDistance = 0;
    var multiplyer = 1;
    var stepDistance = 0;
    var slidesWidth = $('.history-slider__slide').length*$('.history-slider__slide').outerWidth(true);
    var sliderContainerWidth = $('.history-slider').width();

    // Расчитаем длину плашки со шкалой дат (через CSS сделать нельзя, так как flex контейнер в режиме nowrap не растет)
    $('.history-slider__timeline').width(slidesWidth + 250);

    // Плагин добавления панели скроллера
    $('.history-slider__scroll').jScrollPane({
        'contentWidth': slidesWidth + 250,
        'autoReinitialise': true,
        'horizontalDragMaxWidth': 175,
        'showArrows': true
    });
    var scrollApi = $('.history-slider__scroll').data('jsp');


    $(window).resize(function() {
        slidesWidth = $('.history-slider__slide').length*$('.history-slider__slide').outerWidth(true);
        sliderContainerWidth = $('.history-slider').width();
    });

    $('.history-slider__arrow-left, .history-slider__arrow-right').on('click', function(evt) {
        var dir = 1;
        if($(this).data('direction')=='left') {
            dir = -1;
        }
        scrollApi.scrollBy(dir*$('.history-slider__slide').outerWidth(true), 0, true);
    });

    $(".history-slider__scroll").swipe({
        swipeStatus: function (event, phase, direction, distance, duration, fingers, fingerData) {
            if (phase == $.fn.swipe.phases.PHASE_MOVE) {

                if(direction == 'left') {
                    multiplyer = 1;
                } else {
                    multiplyer = -1;
                }

                var blockLeftPos = parseInt($(this).css('left'));

                if(blockLeftPos + multiplyer*(distance - prevDistance) >=0) {
                    //$(this).css('left',0);
                } else if(blockLeftPos + multiplyer*(distance - prevDistance) < -slidesWidth + sliderContainerWidth - 200) {
                    //$(this).css('left', -slidesWidth + sliderContainerWidth - 200);
                } else {
                    //$(this).css('left',blockLeftPos + multiplyer*(distance - prevDistance));

                }
                scrollApi.scrollBy(multiplyer*(distance - prevDistance), 0, false);

                prevDistance = distance;
            }

            if (phase == $.fn.swipe.phases.PHASE_END) {
                prevDistance = 0;
            }

        },
        tap:function(event, target) {
            var linkElem = $(target).parents('a');
            if(linkElem.length != 0) {
                myUtils.sendAjax()('/ajax/news-detail/ajax.php',
                    {"ELEMENT_ID": linkElem.data('id'), "insertNode": "#fullDescription", "LIST_URL": "#historyPage"}
                );

                myUtils.scrollTo("#fullDescription");
            }

        },
        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
           // $(this).text("You swiped " + direction + " with " + fingerCount + " fingers");
        },
        threshold: 1
    });

    // Вызовы AJAX
    $('.history-slider__link').on('click', function(evt) {

    });
});
