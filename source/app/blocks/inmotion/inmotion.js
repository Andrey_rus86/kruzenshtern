$(document).ready(function() {
    var videoSlider = $('.inmotion__video');

    var timeoutHandler = null;

    videoSlider.runVideo = function () {
        clearTimeout(timeoutHandler);
        timeoutHandler = null;

        // Запуск текущего видео
        var currentSlide = videoSlider.slick('slickCurrentSlide');
        var slides = videoSlider.find('.slick-slide');


        var currentSlide = slides.filter(function() {
            return $(this).data("slick-index") == currentSlide
        });
        var currentVideo = currentSlide.children('video');

        $('.inmotion__video-cover').css('background', 'none');
        currentSlide.children('.inmotion__video-cover').addClass('hide');
        currentVideo.attr('controls', 'controls');
        currentVideo[0].play();

        $('.inmotion__play').addClass('displayNone');
    };

    videoSlider.pauseVideo = function () {

        timeoutHandler = setTimeout(function() {
            // Запуск текущего видео
            var currentSlide = videoSlider.slick('slickCurrentSlide');
            var slides = videoSlider.find('.slick-slide');

            var currentSlide = slides.filter(function() {
                return $(this).data("slick-index") == currentSlide
            });
            var currentVideo = currentSlide.children('video');

            currentSlide.children('.inmotion__video-cover').removeClass('hide');
            currentVideo.attr('controls', 'controls');
            currentVideo[0].pause();

            $('.inmotion__play').removeClass('displayNone');
        }, 200);

    };

    videoSlider.find('video').each(function() {
        $(this)[0].onplay = function() {
            videoSlider.runVideo();
        };
        $(this)[0].onpause = function() {
            videoSlider.pauseVideo();
        };
    });

    videoSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        // Отсановить текущее видео
        var slides = videoSlider.find('.slick-slide');

        var currentSlide = slides.filter(function() {
            return $(this).data("slick-index") == currentSlide
        });
        var currentVideo = currentSlide.children('video');

        currentSlide.children('.inmotion__video-cover').removeClass('hide');
        currentVideo.attr('controls', 'controls');
        currentVideo[0].pause();

        $('.inmotion__play').removeClass('displayNone');

        // Меняем превью заголовока следующего видео
        var nextSlide = slides.filter(function() {
            var nextSlideIndex = nextSlide+1;
            if(nextSlideIndex>=slides.length) nextSlideIndex = 0;
            return $(this).data("slick-index") == nextSlideIndex
        });
        $('.inmotion__next-video_title').html(nextSlide.find('.inmotion__video-title').html().replace(/(<([^>]+)>)/ig," "));


    }).slick({
        dots: true,
        infinite: true,
        /*slidesToShow: 1,
        slidesToScroll: 1*/
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });

    $('.inmotion__play').on('click', function() {
        videoSlider.find('.slick-active').children('video')[0].play();
    });

});
