$(document).ready(function () {
    var sea = new google.maps.LatLng(barqueCoords[0],barqueCoords[1]);
    var MY_MAPTYPE_ID = 'custom_style';

    function initialize() {
        var featureOpts = [];

        var mapProp = {
            center: sea,
            zoom: 6,
            scrollwheel: false,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
            },
            mapTypeId: MY_MAPTYPE_ID
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        var styledMapOptions = {
            name: 'Крузенштерн'
        };
        var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

        map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

        var image = {
            url: '/source/build/assets/images/marker.png'
            // scaledSize: new google.maps.Size(46, 65)
        };
        var marker = new google.maps.Marker({
            position: sea,
            map: map,
            icon: image,
            optimized: false // for css control via OverlayView
        });

        var myoverlay = new google.maps.OverlayView();
        myoverlay.draw = function () {
            this.getPanes().markerLayer.id='markerLayer';
        };
        myoverlay.setMap(map);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});