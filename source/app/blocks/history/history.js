$(document).ready(function() {
    var timer = null;
    var sliderElem = $('.history__slider');

    sliderElem.slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });

    // Обработчик события перехода по слайдам
    sliderElem.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        clearTimeout(timer);

        $('.history__text, .history__date').addClass('hide');
        timer = setTimeout(function() {
            $('.history__text-title').html(dataHistory.INFO[nextSlide].TITLESMALL);
            $('.history__description').html(dataHistory.INFO[nextSlide].TXT);
            $('.history__date').html(dataHistory.INFO[nextSlide].DATE);
            $('.history__text, .history__date').removeClass('hide');
        },300);
    });



});