$(document).ready(function() {
    try {
        // Заменяем SVG схемы корабля на встроенный код
        myUtils.svgToInline('#shipSVG');
        var svgInline = $('#shipVector').html();

        // Выводим в области рисования SVG с помощью библиотеки SVG.js
        var draw = SVG('shipScheme').size(1260, 183);
        draw.svg(svgInline);

        // Назначаем слушателей событий внутри SVG дерева
        var rooms = draw.select('.st17, .st16');
        rooms.on('mouseover', function (e) {
            this.addClass('inner__hoverRoom');
        });
        rooms.on('mouseout', function (e) {
            this.removeClass('inner__hoverRoom');
        });

        // Загрузка данных о каюте
        rooms.on('click', function (e) {
            var room_id = this.attr('id');
            loadRoomDescription(room_id);
        });
        $('.inner__roomLink').on('click', function (evt) {
            var room_id = $(this).attr('id');
            loadRoomDescription(room_id);
        });
    } catch (err) {

    }

    function loadRoomDescription(code) {
        myUtils.sendAjax()("/ajax/compartment/ajax.php", {
            "insertNode": "#compartmentDetail",
            "roomCode": code
        });
    }
});

function initFancyBox() {
    $(".fancyboxElem").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'none'
    });

}
