/**
 * Created by Андрей on 20.09.2016.
 */
$(document).ready(function() {

    $('.period__date').on('click', function(evt) {
        $('.period__date').removeClass('selected');
        $(this).addClass('selected');
        myUtils.sendAjax()('/ajax/gallery-list/ajax.php',
            {"ALBUM_CODE": $(this).children('a').data('code'), "insertNode": "#galleryList"}, function() {
                initGallery();
            }
        );
    });

});
