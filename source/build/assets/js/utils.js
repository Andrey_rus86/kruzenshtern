/**
 * Created by Андрей on 20.08.2016.
 */
var Utils = function () {
    // Объект AJAX запроса
    this.XMLHTTPRequest = false;
};

/**
 * Преобразование SVG файла в inline вид
 * @param elemSelector
 */
Utils.prototype.svgToInline = function (elemSelector) {
    $(elemSelector).each(function () {
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.ajax({
            url: imgURL,
            async: false,
            success: function (data) {
                //Get the SVG tag, ignore the rest
                var $svg = $(data).find('svg');
                //Add replaced image's ID to the new SVG
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                //Add replaced image's classes to the new SVG
                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }
                //Remove any invalid XML tags as per http:validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');
                //Replace image with new SVG
                $img.replaceWith($svg);
            }
        });


    });
};

Utils.prototype.sendAjax = function(formData) {

    var self = this;

    return function (path, params,callback) {
        // Проверяем не выполняется ли запрос в данный момент
        if (self.XMLHTTPRequest != false) {
            self.XMLHTTPRequest.abort();
        }

        self.showAjaxLoader();

        var request_params = [];
        var insertNodeSelector = params.insertNode;
        request_params['data'] = params;

        // Если был передан объект класса FormData, то подставить его
        if(formData instanceof FormData) {
            request_params['data'] = formData;
            request_params['processData'] = false;
            request_params['contentType'] = false;
        }

        request_params['type'] = 'POST';
        request_params['success'] = function (data) {
            $(insertNodeSelector).html($.trim(data));
            self.hideAjaxLoader();
            self.XMLHTTPRequest = false;
            if(callback != undefined) {
                callback();
            }
        };

        self.XMLHTTPRequest = $.ajax(path, request_params);
    }
};

Utils.prototype.showAjaxLoader = function showAjaxLoader() {
    $('#mask').height($(document).height());
    $('#loading-light, #mask').css('display', 'block');
};

Utils.prototype.hideAjaxLoader = function hideAjaxLoader() {
    $('#loading-light, #mask').css('display', 'none');
}

Utils.prototype.scrollTo = function scrollTo(selector) {
    $('html, body').animate({
        scrollTop: $(selector).offset().top + 'px'
    }, 'fast');
}

var myUtils = new Utils();