$(document).ready(function() {
    var coord1 = new google.maps.LatLng(54.739401, 20.508661);
    var coord2 = new google.maps.LatLng(54.737598, 20.514967);
    var coord3 = new google.maps.LatLng(54.738639, 20.514903);

    var MY_MAPTYPE_ID = 'custom_style';

    function initialize() {
        try {
            var featureOpts = [];

            var mapProp = {
                center: coord1,
                zoom: 14,
                scrollwheel: false,
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
                },
                mapTypeId: MY_MAPTYPE_ID
            };
            var map = new google.maps.Map(document.getElementById("cartMap"), mapProp);

            var styledMapOptions = {
                name: 'Мы на карте'
            };
            var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

            map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

            var image = {
                //url: '/source/build/assets/images/marker.png'
                //scaledSize: new google.maps.Size(46, 65)
            };

            var marker1 = new google.maps.Marker({
                position: coord1,
                map: map,
                icon: image
            });

            var marker2 = new google.maps.Marker({
                position: coord2,
                map: map,
                icon: image
            });

            var marker3 = new google.maps.Marker({
                position: coord3,
                map: map,
                icon: image
            });
        } catch (err) {

        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);

});
;$(document).ready(function() {
    var timer = null;
    $('.characteristics__dot').on('mouseover click', function(evt) {
        var index = $(this).data('index');
        $('.characteristics__detail').addClass('hide');
        timer = setTimeout(function() {
            if(dataCharacteristics.ITEMS[index].IMAGE != '') {
                $('.characteristics__detail-image').css('display','block');
                $('.characteristics__detail-image').attr('src', dataCharacteristics.ITEMS[index].IMAGE);
            } else {
                $('.characteristics__detail-image').css('display','none');
            }
            $('.characteristics__detail-name').html(dataCharacteristics.ITEMS[index].DETAIL_NAME);
            $('.characteristics__detail-text').html(dataCharacteristics.ITEMS[index].DETAIL_TEXT);


            for (var i = 0; i < dataCharacteristics.ITEMS[index].PROPS.length; ++i) {
                $('.characteristics__detail-text').append("<p class='detail-p'>"+dataCharacteristics.ITEMS[index].PROPS[i]+"</p>");
            }

            $('.characteristics__detail').removeClass('hide');
        }, 400);

    });
});;$(document).ready(function(){
    // Инициализация плагина Popup окна
    $(".fancybox").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'none',
        aspectRatio : true,
        autoWidth: true,
        autoHeight: true,
        afterShow: function () {

            var fancyboxContainer = $('.fancybox-opened video');
            if(fancyboxContainer.length>0) {
                $.fancybox.update();
            }

        }
    });
});

$(window).load(function() {
    initGallery();
});

function initGallery() {
    // Подгружаем изображения
    $('.gallery-slider__slider').each(function() {
       var $images = $(this).find('img');
        var $countImages = $images.length;
        var $loadedImages = 0;
        var $allLoaded = false;
        var $slider = $(this);
        $images.each(function() {
            /*$(this)[0].onload = function() {
                $loadedImages++;
                if($loadedImages == $countImages) {

                }
            };*/
            $(this).attr('src', $(this).data('src'));
        });
        initPlugins($slider);
    });

    function initPlugins(container) {
        $slickSlider = container;
        // Инициализвция плагина слайдера
        $slickSlider.slick({
            infinite: false,
            slidesToShow: 2.5,
            slidesToScroll: 1,
            swipe: false
        });

        // Инициализация плагина прокрутки
        $scrollPane = $slickSlider.find('.slick-list').jScrollPane();

        // Переписываем функционал поведения слайдера при нажатии на кнопки прокрутки
        $('.gallery-slider button.slick-next.slick-arrow').off('click');
        $('.gallery-slider button.slick-next.slick-arrow').on('click', function(event) {
            event.preventDefault();
            $parentSilder = $(this).parents('.gallery-slider__slider');
            var slideWidth = $parentSilder.find('.gallery-slider__slide').outerWidth(true);
            var scrollApi = $parentSilder.find('.jspScrollable').data('jsp');

            scrollApi.scrollBy(slideWidth, 0, true);
        });

        $('.gallery-slider button.slick-prev.slick-arrow').off('click');
        $('.gallery-slider button.slick-prev.slick-arrow').on('click', function(event) {
            event.preventDefault();
            $parentSilder = $(this).parents('.gallery-slider__slider');
            var slideWidth = $parentSilder.find('.gallery-slider__slide').outerWidth(true);
            var scrollApi = $parentSilder.find('.jspScrollable').data('jsp');

            scrollApi.scrollBy(-slideWidth, 0, true);

        });
    }


};$(document).ready(function () {
    // Плагин добавления swipe функционала для блока
    var prevDistance = 0;
    var multiplyer = 1;
    var stepDistance = 0;
    var slidesWidth = $('.history-slider__slide').length*$('.history-slider__slide').outerWidth(true);
    var sliderContainerWidth = $('.history-slider').width();

    // Расчитаем длину плашки со шкалой дат (через CSS сделать нельзя, так как flex контейнер в режиме nowrap не растет)
    $('.history-slider__timeline').width(slidesWidth + 250);

    // Плагин добавления панели скроллера
    $('.history-slider__scroll').jScrollPane({
        'contentWidth': slidesWidth + 250,
        'autoReinitialise': true,
        'horizontalDragMaxWidth': 175,
        'showArrows': true
    });
    var scrollApi = $('.history-slider__scroll').data('jsp');


    $(window).resize(function() {
        slidesWidth = $('.history-slider__slide').length*$('.history-slider__slide').outerWidth(true);
        sliderContainerWidth = $('.history-slider').width();
    });

    $('.history-slider__arrow-left, .history-slider__arrow-right').on('click', function(evt) {
        var dir = 1;
        if($(this).data('direction')=='left') {
            dir = -1;
        }
        scrollApi.scrollBy(dir*$('.history-slider__slide').outerWidth(true), 0, true);
    });

    $(".history-slider__scroll").swipe({
        swipeStatus: function (event, phase, direction, distance, duration, fingers, fingerData) {
            if (phase == $.fn.swipe.phases.PHASE_MOVE) {

                if(direction == 'left') {
                    multiplyer = 1;
                } else {
                    multiplyer = -1;
                }

                var blockLeftPos = parseInt($(this).css('left'));

                if(blockLeftPos + multiplyer*(distance - prevDistance) >=0) {
                    //$(this).css('left',0);
                } else if(blockLeftPos + multiplyer*(distance - prevDistance) < -slidesWidth + sliderContainerWidth - 200) {
                    //$(this).css('left', -slidesWidth + sliderContainerWidth - 200);
                } else {
                    //$(this).css('left',blockLeftPos + multiplyer*(distance - prevDistance));

                }
                scrollApi.scrollBy(multiplyer*(distance - prevDistance), 0, false);

                prevDistance = distance;
            }

            if (phase == $.fn.swipe.phases.PHASE_END) {
                prevDistance = 0;
            }

        },
        tap:function(event, target) {
            var linkElem = $(target).parents('a');
            if(linkElem.length != 0) {
                myUtils.sendAjax()('/ajax/news-detail/ajax.php',
                    {"ELEMENT_ID": linkElem.data('id'), "insertNode": "#fullDescription", "LIST_URL": "#historyPage"}
                );

                myUtils.scrollTo("#fullDescription");
            }

        },
        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
           // $(this).text("You swiped " + direction + " with " + fingerCount + " fingers");
        },
        threshold: 1
    });

    // Вызовы AJAX
    $('.history-slider__link').on('click', function(evt) {

    });
});
;$(document).ready(function() {
    var timer = null;
    var sliderElem = $('.history__slider');

    sliderElem.slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });

    // Обработчик события перехода по слайдам
    sliderElem.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        clearTimeout(timer);

        $('.history__text, .history__date').addClass('hide');
        timer = setTimeout(function() {
            $('.history__text-title').html(dataHistory.INFO[nextSlide].TITLESMALL);
            $('.history__description').html(dataHistory.INFO[nextSlide].TXT);
            $('.history__date').html(dataHistory.INFO[nextSlide].DATE);
            $('.history__text, .history__date').removeClass('hide');
        },300);
    });



});;$(document).ready(function() {
    var videoSlider = $('.inmotion__video');

    var timeoutHandler = null;

    videoSlider.runVideo = function () {
        clearTimeout(timeoutHandler);
        timeoutHandler = null;

        // Запуск текущего видео
        var currentSlide = videoSlider.slick('slickCurrentSlide');
        var slides = videoSlider.find('.slick-slide');


        var currentSlide = slides.filter(function() {
            return $(this).data("slick-index") == currentSlide
        });
        var currentVideo = currentSlide.children('video');

        $('.inmotion__video-cover').css('background', 'none');
        currentSlide.children('.inmotion__video-cover').addClass('hide');
        currentVideo.attr('controls', 'controls');
        currentVideo[0].play();

        $('.inmotion__play').addClass('displayNone');
    };

    videoSlider.pauseVideo = function () {

        timeoutHandler = setTimeout(function() {
            // Запуск текущего видео
            var currentSlide = videoSlider.slick('slickCurrentSlide');
            var slides = videoSlider.find('.slick-slide');

            var currentSlide = slides.filter(function() {
                return $(this).data("slick-index") == currentSlide
            });
            var currentVideo = currentSlide.children('video');

            currentSlide.children('.inmotion__video-cover').removeClass('hide');
            currentVideo.attr('controls', 'controls');
            currentVideo[0].pause();

            $('.inmotion__play').removeClass('displayNone');
        }, 200);

    };

    videoSlider.find('video').each(function() {
        $(this)[0].onplay = function() {
            videoSlider.runVideo();
        };
        $(this)[0].onpause = function() {
            videoSlider.pauseVideo();
        };
    });

    videoSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        // Отсановить текущее видео
        var slides = videoSlider.find('.slick-slide');

        var currentSlide = slides.filter(function() {
            return $(this).data("slick-index") == currentSlide
        });
        var currentVideo = currentSlide.children('video');

        currentSlide.children('.inmotion__video-cover').removeClass('hide');
        currentVideo.attr('controls', 'controls');
        currentVideo[0].pause();

        $('.inmotion__play').removeClass('displayNone');

        // Меняем превью заголовока следующего видео
        var nextSlide = slides.filter(function() {
            var nextSlideIndex = nextSlide+1;
            if(nextSlideIndex>=slides.length) nextSlideIndex = 0;
            return $(this).data("slick-index") == nextSlideIndex
        });
        $('.inmotion__next-video_title').html(nextSlide.find('.inmotion__video-title').html().replace(/(<([^>]+)>)/ig," "));


    }).slick({
        dots: true,
        infinite: true,
        /*slidesToShow: 1,
        slidesToScroll: 1*/
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });

    $('.inmotion__play').on('click', function() {
        videoSlider.find('.slick-active').children('video')[0].play();
    });

});
;$(document).ready(function() {
    try {
        // Заменяем SVG схемы корабля на встроенный код
        myUtils.svgToInline('#shipSVG');
        var svgInline = $('#shipVector').html();

        // Выводим в области рисования SVG с помощью библиотеки SVG.js
        var draw = SVG('shipScheme').size(1260, 183);
        draw.svg(svgInline);

        // Назначаем слушателей событий внутри SVG дерева
        var rooms = draw.select('.st17, .st16');
        rooms.on('mouseover', function (e) {
            this.addClass('inner__hoverRoom');
        });
        rooms.on('mouseout', function (e) {
            this.removeClass('inner__hoverRoom');
        });

        // Загрузка данных о каюте
        rooms.on('click', function (e) {
            var room_id = this.attr('id');
            loadRoomDescription(room_id);
        });
        $('.inner__roomLink').on('click', function (evt) {
            var room_id = $(this).attr('id');
            loadRoomDescription(room_id);
        });
    } catch (err) {

    }

    function loadRoomDescription(code) {
        myUtils.sendAjax()("/ajax/compartment/ajax.php", {
            "insertNode": "#compartmentDetail",
            "roomCode": code
        });
    }
});

function initFancyBox() {
    $(".fancyboxElem").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'none'
    });

}
;$(document).ready(function () {
    var sea = new google.maps.LatLng(barqueCoords[0],barqueCoords[1]);
    var MY_MAPTYPE_ID = 'custom_style';

    function initialize() {
        var featureOpts = [];

        var mapProp = {
            center: sea,
            zoom: 6,
            scrollwheel: false,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
            },
            mapTypeId: MY_MAPTYPE_ID
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        var styledMapOptions = {
            name: 'Крузенштерн'
        };
        var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

        map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

        var image = {
            url: '/source/build/assets/images/marker.png'
            // scaledSize: new google.maps.Size(46, 65)
        };
        var marker = new google.maps.Marker({
            position: sea,
            map: map,
            icon: image,
            optimized: false // for css control via OverlayView
        });

        var myoverlay = new google.maps.OverlayView();
        myoverlay.draw = function () {
            this.getPanes().markerLayer.id='markerLayer';
        };
        myoverlay.setMap(map);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});;$(document).ready(function() {
    $('.partners__slider').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000
    });
});
;/**
 * Created by Андрей on 20.09.2016.
 */
$(document).ready(function() {

    $('.period__date').on('click', function(evt) {
        $('.period__date').removeClass('selected');
        $(this).addClass('selected');
        myUtils.sendAjax()('/ajax/gallery-list/ajax.php',
            {"ALBUM_CODE": $(this).children('a').data('code'), "insertNode": "#galleryList"}, function() {
                initGallery();
            }
        );
    });

});
;/**
 * Created by Андрей on 03.09.2016.
 */
$(document).ready(function() {
    $(".fancyboxElem").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'none'
    });
});

;/**
 * Created by Андрей on 22.04.2016.
 */
$(document).ready(function() {

});