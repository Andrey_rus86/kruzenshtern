<?
use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;

Loader::includeModule('iblock');



// Выбираем информацию по инфоблоку с целью получить путь
$iblockData = IblockTable::getList([
	'select' => ['*'],
	'filter' => ['ID' => Keypoint\Utils\Iblock::getIblockIdByCode('gallery')]
])->fetch();

$iblockData['SECTION_PAGE_URL'] = preg_replace('#\#SITE_DIR\##','',$iblockData['SECTION_PAGE_URL']);


// Получаем список рпзделов галереи
$resSection = SectionTable::getList([
	'select' => ['NAME', 'ID', 'CODE'],
	'filter' => ['IBLOCK_ID' => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'), 'PARENT_SECTION.CODE'  => 'gallery'],
	'limit' => 5,
	'order' => ['SORT' => 'ASC']
]);

$subLevelMenu = [];
while($dataSection = $resSection->fetch()) {
	// Подстановка URL
	$url = preg_replace('#\#CODE\##',$dataSection['CODE'],$iblockData['SECTION_PAGE_URL']);

	$subLevelMenu[] = [
		$dataSection['NAME'], $url, [],[]
	];
}


// Подстановка URL по параметрам
//$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavParams, $arSelect);
//$rsElement->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);


$aMenuLinks = array_merge($aMenuLinks, $subLevelMenu);

?>