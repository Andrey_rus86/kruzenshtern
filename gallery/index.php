<?
use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Барк «Крузенштерн» галерея");
$APPLICATION->SetTitle("Барк «Крузенштерн» галерея");

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH .'/css/gallery.css');
?>

<?
ob_start();
$APPLICATION->IncludeFile(
    "/include_areas/".LANGUAGE_ID."/gallery/intro.php",
    Array(),
    Array("MODE"=>"html")
);
$intro = ob_get_clean();
ob_start();
$APPLICATION->IncludeFile(
    "/include_areas/".LANGUAGE_ID."/gallery/title.php",
    Array(),
    Array("MODE"=>"html")
);
$title = ob_get_clean();

echo $GLOBALS['JADE_RENDERER']->render(LAYOUT_PATH.'blocks/gallery-about/gallery-about.jade',
    [
        'galleryabout' => [
            'TITLE' => $title,
            'TEXT' => $intro
        ]
    ]);

?>
<?
// Список дат
?>
<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list","period",
    Array(
        "VIEW_MODE" => "TEXT",
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "services",
        "IBLOCK_ID" => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
        "SECTION_ID" => "",
        "SECTION_CODE" => "gallery",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "1",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => "",
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "Y"
    )
);?>


<div id="galleryList">
    <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list","albums",
        Array(
            "VIEW_MODE" => "TEXT",
            "SHOW_PARENT_NAME" => "Y",
            "IBLOCK_TYPE" => "services",
            "IBLOCK_ID" => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
            "SECTION_ID" => "",
            "SECTION_CODE" => isset($_REQUEST['SECTION_CODE']) ? htmlspecialchars($_REQUEST['SECTION_CODE']): 'year2006',
            "SECTION_URL" => "",
            "COUNT_ELEMENTS" => "Y",
            "TOP_DEPTH" => "1",
            "SECTION_FIELDS" => "",
            "SECTION_USER_FIELDS" => "",
            "ADD_SECTIONS_CHAIN" => "Y",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_NOTES" => "",
            "CACHE_GROUPS" => "Y"
        )
    );?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>