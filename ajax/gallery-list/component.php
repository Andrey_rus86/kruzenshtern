<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list","albums",
    Array(
        "VIEW_MODE" => "TEXT",
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "services",
        "IBLOCK_ID" => Keypoint\Utils\Iblock::getIblockIdByCode('gallery'),
        "SECTION_ID" => "",
        "SECTION_CODE" => $_REQUEST['ALBUM_CODE'],
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "1",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => "",
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "Y"
    )
);?>